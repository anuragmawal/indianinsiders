<footer class="footer transition">
  <div class="footer-bar">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          <ul style="display: block;margin: 0px auto;width: 183px;" class="social-media">
			 <li><a target="_blank" href="https://www.facebook.com/indianinsiders"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				  <li><a target="_blank" href="https://twitter.com/indianinsiders"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				  <li><a target="_blank" href="https://www.linkedin.com/company/27148513/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          </ul>
        
        </div>
		 <div class="col-md-3 col-sm-6 col-xs-12">
          <address>
          <i class="fa fa-phone" aria-hidden="true"></i> +33 601 86 78 38 </br> +91 95 45 54 00 00 <small>Any questions? Call us.</small>
          </address>
        </div>
		<div class="col-md-3 col-sm-6 col-xs-12">
          <address>
          <i class="fa fa-map-marker" aria-hidden="true"></i> Paris, France & Pune, India
          </address>
        </div>
      </div>
   
    </div>
 
  </div>
  <!-- end footer-bar -->
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="text-center col-xs-12">Copyright © 2018 , Indian Insiders</div>
        <!-- end col-6 -->
       
        <!-- end col-6 --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </div>
  <!-- end sub-footer --> 
</footer>
<!-- end footer --> 
<a href="#" class="scrollup"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a> 

<!-- JS FILES --> 
<script src="js/jquery.min.js"></script> 
<script type="text/javascript">
$(window).load(function() {
	$(".preloader").fadeOut("fast");
})
</script> 
<script src="js/analytics.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jPushMenu.js"></script> 
<script src="js/odometer.min.js"></script> 
<script src="js/owl.carousel.min.js"></script> 
<script src="js/jquery.fancybox.js"></script> 
<script src="js/wow.min.js"></script> 
<script src="js/masonry.pkgd.min.js"></script> 
<script src="js/scripts.js"></script>

<!-- REVOLUTION JS FILES --> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script> 
<script type='text/javascript' src='revolution/js/revolution.addon.slicey.min.js?ver=1.0.0'></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script> 

<script type="text/javascript">
		
			var tpj=jQuery;
			var revapi24;
			tpj(document).ready(function() {
				if(tpj("#rev_slider_24_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_24_1");
				}else{
					revapi24 = tpj("#rev_slider_24_1").show().revolution({
						sliderType:"standard",
						jsFileLocation:"revolution/js/",
						sliderLayout:"fullscreen",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
 							mouseScrollReverse:"default",
							onHoverStop:"off",
							bullets: {
								enable:true,
								hide_onmobile:false,
								style:"bullet-bar",
								hide_onleave:false,
								direction:"horizontal",
								h_align:"center",
								v_align:"bottom",
								h_offset:0,
								v_offset:50,
								space:5,
								tmp:''
							}
						},
						responsiveLevels:[1240,1024,778,480],
						visibilityLevels:[1240,1024,778,480],
						gridwidth:[1240,1024,778,480],
						gridheight:[868,768,960,720],
						lazyType:"none",
						shadow:0,
						spinner:"off",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						fullScreenAutoWidth:"off",
						fullScreenAlignForce:"off",
						fullScreenOffsetContainer: "",
						fullScreenOffset: "0",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}

                if(revapi24) revapi24.revSliderSlicey();
				
				$("#slide-67-layer-7").click(function(){
					 $("html, body").animate({ scrollTop: $(".header").height() }, 1000);
				});
			});	/*ready*/
		</script>
</body>
</html>