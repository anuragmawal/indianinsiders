<!DOCTYPE html>
<html lang="en"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Indian Insiders</title>
<meta name="author" content="IndianInsiders">
<meta name="description" content="Indian Insiders helps you to understand the Indian market by providing a personalized assessment of your product in India.">
<meta name="keywords" content="business, corporate, Indian Market, Fastest growing economy, Global, Consultants">

<!-- SOCIAL MEDIA META -->
<meta property="og:description" content="Indian Insiders helps you to understand the Indian market by providing a personalized assessment of your product in India.">
<meta property="og:image" content="images/hero1.jpg">
<meta property="og:title" content="Indian Insiders">
<meta property="og:type" content="Consulting">
<meta property="og:url" content="http://www.indianinsiders.com">

<!-- TWITTER META -->
<meta name="twitter:card" content="Indian Insiders helps you to understand the Indian market by providing a personalized assessment of your product in India.">
<meta name="twitter:site" content="http://www.indianinsiders.com">
<meta name="twitter:creator" content="http://www.indianinsiders.com">
<meta name="twitter:title" content="Indian Insiders">
<meta name="twitter:description" content="Indian Insiders helps you to understand the Indian market by providing a personalized assessment of your product in India.">
<meta name="twitter:image" content="http://www.indianinsiders.com">

<!-- FAVICON FILES -->
<link href="images/favicon.png" rel="apple-touch-icon" sizes="144x144">
<link href="images/favicon.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/favicon.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/favicon.png" rel="apple-touch-icon">
<link href="images/favicon.png" rel="shortcut icon">


<!-- CSS FILES -->
<link href="revolution/css/settings.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/jquery.fancybox.css" rel="stylesheet">
<link href="css/odometer.min.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/base.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="preloader"> <svg class="spinner" id="pageloader_anime" width="32px" height="32px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
  <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
  </svg> </div>
<!-- end preloader -->
<div class="search-box transition">
   <div class="table">
   <div class="table-cell">
    <div class="container">
    	<form>
    		<input type="text" placeholder="Type here to make a search" autofocus>
    		<button type="submit">SEARCH</button>
    	</form>
    	</div>
    	<!-- end container -->
    	</div>
    	<!-- end table-cell -->
    	</div>
    	<!-- end table -->
    </div>
    <!-- end search-box -->
<header class="header">
  <nav class="navbar navbar-default navbar-fixed-top transition">
  <!-- end top-bar -->
    <div class="container">
      <div class="navbar-header">
        <!--<button type="button" class="navbar-toggle toggle-menu menu-left push-body" data-toggle="collapse" data-target="#collapse-nav"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>-->
        <a class="navbar-brand" href="index"> <img src="images/logo-light.png" alt="Image" class="logo-light"> <img src="images/logo-dark.png" alt="Image" class="logo-dark"></a>
		<ul class="nav navbar-nav visible-xs hidden-sm">
			<!--<li class="mbastudentslink"><a style="font-size: 0.8em;" target="_blank" href="contact.html">Contct</a></li>
			<li class="mbastudentslink"><a style="font-size: 0.8em;" target="_blank" href="blog.html">Blog</a></li>
			<li class="mbastudentslink"><a style="font-size: 0.8em;" target="_blank" href="students">MBA Students</a></li>-->
			<li class="mbastudentslink"><a style="font-size: 0.8em;" target="_blank" href="contact ">Contact Us</a></li>
		</ul>	
		</div>
      <div class="collapse navbar-collapse cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="collapse-nav">
        <ul class="top-menu visible-xs">
		  <li><a href="our-offices.html">Our Offices</a></li>
		  <li><a href="career.html">Career</a></li>
		  <li><a href="on-press.html">On Press</a></li>
		  <li><a href="support.html">Support</a></li>
		</ul>
            <!-- end top-menu --> 
        <ul class="nav navbar-nav">
          <li style="visibility:hidden;"><a href="index.html">HOME</a></li>
          <li style="visibility:hidden;" class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">PAGES</a>
            <ul class="dropdown-menu">
              <li><a href="about-us.html">About Us</a></li>
              <li><a href="solutions.html">Solutions</a></li>
              <li><a href="partners.html">Partners</a></li>
              <li><a href="gallery.html">Gallery</a></li>
              <li><a href="pricing.html">Pricing</a></li>
              <li><a href="faq.html">FAQ</a></li>
              <li><a href="elements.html">Elements</a></li>
              <li><a href="404.html">404 Page</a></li>
            </ul>
          </li>
          <li style="display:none;"><a href="projects.html">PROJECTS</a></li>
          <!--<li><a target="_blank" href="students">MBA Students</a></li>-->
		  <!--<li><a href="blog.html">Blog</a></li>
          <li><a href="contact.html">Contact</a></li>-->
        </ul>
        <!-- end navbar-nav --> 
         <a href="contact" class="quote-btn">CONTACT US</a>
        <span class="phone"><i class="fa fa-phone" aria-hidden="true"></i> +33 601 86 78 38 / +91 95 45 54 00 00</span> 
        <!-- end phone --> 
        <!--<div class="search"><i class="fa fa-search" aria-hidden="true"></i></div>-->
        <!-- end search -->
      </div>
      <!-- end navbar-collapse --> 
    </div>
    <!-- end container --> 
  </nav>
  <!-- end navbar-default -->
  <div id="rev_slider_24_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="website-intro" data-source="gallery" >
    <div id="rev_slider_24_1" class="rev_slider fullscreenbanner tiny_bullet_slider" data-version="5.4.1">
      <ul>
        <li data-index="rs-67" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="600"  data-thumb="../../assets/images/woman2-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="" data-slicey_shadow="0px 0px 0px 0px transparent"> <img src="images/hero5.jpg"  alt="Image"  data-bgposition="center center" data-kenburns="on" data-duration="5000" data-ease="Power2.easeInOut" data-scalestart="100" data-scaleend="150" data-rotatestart="0" data-rotateend="0" data-blurstart="20" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-9" 
			data-x="['center','center','center','center']" data-hoffset="['-112','-43','-81','44']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['-219','-184','-185','182']" 
			data-width="['250','250','150','150']"
			data-height="['150','150','100','100']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 		
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":300,"speed":1000,"frame":"0","from":"rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3700","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"		
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 5;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-10" 
			data-x="['center','center','center','center']" data-hoffset="['151','228','224','117']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['-212','-159','71','-222']" 
			data-width="['150','150','100','100']"
			data-height="['200','150','150','150']"
			data-whitespace="nowrap"
		 	data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":350,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3650","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 6;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-29" 
			data-x="['center','center','center','center']" data-hoffset="['339','-442','104','-159']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['2','165','-172','219']" 
			data-width="['250','250','150','150']"
			data-height="['150','150','100','100']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":400,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3600","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 7;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-12" 
			data-x="['center','center','center','center']" data-hoffset="['162','216','-239','193']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['195','245','6','146']" 
			data-width="['250','250','100','100']"
			data-height="150"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":450,"speed":1000,"frame":"0","from":"opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3550","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 8;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-34" 
			data-x="['center','center','center','center']" data-hoffset="['-186','-119','273','-223']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['269','217','-121','69']" 
			data-width="['300','300','150','150']"
			data-height="['200','200','150','150']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3500","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 9;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-11" 
			data-x="['center','center','center','center']" data-hoffset="['-325','292','162','-34']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['3','55','-275','-174']" 
			data-width="150"
			data-height="['250','150','50','50']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":550,"speed":1000,"frame":"0","from":"opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3450","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 10;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-27" 
			data-x="['center','center','center','center']" data-hoffset="['-429','523','-190','-306']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['-327','173','181','480']" 
			data-width="['250','250','150','150']"
			data-height="['300','300','150','150']"
			data-whitespace="nowrap"
		 	data-type="shape" 
			data-slicey_offset="300" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":320,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3680","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 11;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-28" 
			data-x="['center','center','center','center']" data-hoffset="['422','-409','208','225']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['-245','-72','294','-14']" 
			data-width="['300','300','150','150']"
			data-height="['250','250','100','100']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="300" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":360,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3640","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 12;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-30" 
			data-x="['center','center','center','center']" data-hoffset="['549','-445','28','58']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['236','400','316','287']" 
			data-width="['300','300','150','200']"
			data-height="['250','250','150','50']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="300" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":400,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3600","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 13;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-31" 
			data-x="['center','center','center','center']" data-hoffset="['-522','492','-151','262']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['339','-180','330','-141']" 
			data-width="['300','300','150','150']"
			data-height="['250','250','100','100']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="300" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":440,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3560","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 14;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-32" 
			data-x="['center','center','center','center']" data-hoffset="['-588','-375','-253','-207']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['72','-328','-172','-111']" 
			data-width="['300','300','150','150']"
			data-height="['200','200','150','150']"
			data-whitespace="nowrap"
		 	data-type="shape" 
			data-slicey_offset="300" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":480,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3520","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 15;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-33" 
			data-x="['center','center','center','center']" data-hoffset="['-37','73','-76','-100']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['-401','-340','-293','-246']" 
			data-width="['450','400','250','250']"
			data-height="['100','100','50','50']"
			data-whitespace="nowrap"
		 	data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 
			data-responsive_offset="on" 
			data-frames='[{"delay":310,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3690","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 16;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper tp-slicey  tp-resizeme" 
			id="slide-67-layer-35" 
			data-x="['center','center','center','center']" data-hoffset="['186','38','116','17']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['363','402','190','395']" 
			data-width="['350','400','250','250']"
			data-height="['100','100','50','50']"
			data-whitespace="nowrap"
			data-type="shape" 
			data-slicey_offset="250" 
			data-slicey_blurstart="0" 
			data-slicey_blurend="20" 	
			data-responsive_offset="on" 
			data-frames='[{"delay":340,"speed":1000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"+3660","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 17;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption tp-shape tp-shapewrapper " 
			id="slide-67-layer-1" 
			data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
			data-width="full"
			data-height="full"
			data-whitespace="nowrap"
		 	data-type="shape" 
			data-basealign="slide" 
			data-responsive_offset="off" 
			data-responsive="off"
			data-frames='[{"delay":10,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":500,"frame":"999","to":"opacity:0;","ease":"Power4.easeOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 18;background-color:rgba(0, 0, 0, 0.5);"> </div>
          <div class="tp-caption main-title maintitleheading  tp-resizeme" 
			id="slide-67-layer-2" 
			data-x="['center','center','center','center']" data-hoffset="['1','1','0','0']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['-70','-70','-70','-70']" 
			data-fontsize="['90','90','70','50']"
			data-lineheight="['90','90','70','50']"
			data-width="['none','none','481','360']"
			data-height="none"
			data-whitespace="['nowrap','nowrap','normal','normal']"
		 	data-type="text" 
			data-responsive_offset="on" 
			data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"}]'
			data-textAlign="['center','center','center','center']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 19; white-space: nowrap; font-size: 90px; line-height: 90px; font-weight: 500; color: #ffffff; letter-spacing: -5px; ">India is ready for your business,<br/>
            but are you ready for India?</div>
          <div class="tp-caption subheading tp-resizeme" 
			id="slide-67-layer-3" 
			data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['90','90','60','30']" 
			data-fontsize="['25','25','25','20']"
			data-lineheight="['35','35','35','30']"
			data-width="['480','480','480','360']"
			data-height="none"
			data-whitespace="normal"
			data-type="text" 
			data-responsive_offset="on" 
			data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"}]'
			data-textAlign="['center','center','center','center']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"
			style="z-index: 20; min-width: 480px; max-width: 480px; white-space: normal; font-size: 25px; line-height: 35px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">Get your product validated in India and be part of the world’s fastest-growing economy.</div>
          <a class="tp-caption rev-btn knowmorebutton tp-resizeme" 
		 href="#" id="slide-67-layer-7" 
			data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['200','200','160','120']" 
			data-width="250"
			data-height="none"
			data-whitespace="nowrap"
			data-type="button" 
			data-actions=''
			data-responsive_offset="on" 
			data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;","style":"c:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
			data-textAlign="['center','center','center','center']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[50,50,50,50]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[50,50,50,50]"
			style="z-index: 21; white-space: nowrap; cursor:pointer;text-decoration: none;">KNOW MORE</a> </li>
      </ul>
      <div class="tp-bannertimer tp-bottom" style="height: 3px; background:#8a39ee"></div>
    </div>
  </div>
  <!-- end revslider --> 
  
</header>
<!-- end header -->

<section class="featured-solutions transition setbackgroundcolornew">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 text-center">
        <h6>Our Mission</h6>
        <h2>Indian Insiders helps you to understand the Indian market by providing a personalized assessment of your product in India.</h2>
		 <h6>We help in</h6>
      </div>
      <!-- end col-12 -->
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="solution-box">
          <figure class="image"><img src="images/hero3.jpg" alt="Image"></figure>
          <!-- end image -->
          <h4>Testing your product or service in the Indian market.</h4>
          <p>We help you validate your product in the Indian market with the help of highly motivated students from top business schools in India.</p>
        </div>
        <!-- end solution-box --> 
      </div>
      <!-- end col-4 -->
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="solution-box">
          <figure class="image"><img src="images/hero2.jpg" alt="Image"></figure>
          <!-- end image -->
          <h4>Starting or growing your business in India.</h4>
          <p>Once you know the Indian market, we connect you to the right people in the Indian government and other agencies for your product launch.</p>
        </div>
        <!-- end solution-box --> 
      </div>
      <!-- end col-4 -->
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="solution-box">
          <figure class="image"><img src="images/hero1.jpg" alt="Image"></figure>
          <!-- end image -->
          <h4>Finding and building your team in India.</h4>
					<p>The hardest part is to find the best team. Worry not, we got you covered. We help you find the best people to work with you in India.</p>
        </div>
        <!-- end solution-box --> 
      </div>
      <!-- end col-4 -->
      <!-- end col-12 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>

<section class="highlights">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h6>Why us?</h6>
        <h2>Our unique analysis helps in validating your product/services in Indian market with potential clients and users.</h2>
      </div>
      <!-- end col-12 -->
      <div class="col-md-2 col-md-offset-3 col-sm-4 col-xs-12"> <img src="images/icon1.jpg" alt="Image">
        <h4>FASTER</h4>
        <p>Get multiple analysis from top business school students and test run your product without any delay. </p>
      </div>
      <!-- end col-2 -->
      <div class="col-md-2 col-sm-4 col-xs-12"> <img src="images/icon2.jpg" alt="Image">
        <h4>CHEAPER</h4>
        <p>Get your idea tested in the Indian market without spending a huge amount of money and avoiding any financial risk.</p>
      </div>
      <!-- end col-2 -->
      <div class="col-md-2 col-sm-4 col-xs-12"> <img src="images/icon3.jpg" alt="Image">
        <h4>RELIABLE</h4>
        <p>Our consultants are students from top Indian business schools and our connects with the government agencies makes working in India easier.</p>
      </div>
      <!-- end col-2 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>

<!--<section class="left-side-content transition">
 <div class="side-image"> <img src="images/side-image1.jpg" alt="Image" class="bg-image"> 
   </div>
<div class="container">
    <div class="row">
      <div class="col-md-4">
        <h6>Why India? </h6>
        <h2>6th largest economy; 2 Trillion dollars market opportunity & 7.5% growth rate. </h2>
        <p>India is a large demographic with a population of 1.3 billion people.
				</br>Spending power of average Indian is on the rise and so are the needs.
			</br>Over 500 million people are under the age of 35 and by 2020 we will have world’s largest youth population and will become the largest supplier of university graduates.
</br><b>This is the time to enter Indian market. Our job is to help you understand Indian market and harness its potential.</b></p>
       </div>
    
      <div class="col-md-6 col-md-offset-2">
     
    </div>
</div>

</section>-->
<!-- end work-process -->
<section class="featured-services transition">
  <div class="container">
    <div class="row">
			<!-- end col-4 -->
			<div class="col-xs-12 text-center">
				 <h6>Our Services</h6>
        <h2>Our market research involves an in-depth assessment of </h2>
			</div>
      <div class="col-md-12  col-sm-12 col-xs-12">
        <div class="row inner">
				<div class="col-md-4 col-sm-6 col-xs-12">
            <h4>Current and future opportunity</h4>
            <p>Understand the opportunities in India</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12"> 
            <h4>Government Regulations</h4>
            <p> About your product and industry.</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12"> 
            <h4>Competitor analysis</h4>
            <p>Know competitor landscape</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h4>Entry barriers</h4>
            <p>Understand the entry barries in market.</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12"> 
            <h4>Technology trends</h4>
            <p>Understand the new Technology trends.</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h4>Demand & supply gaps</h4>
            <p>Know the gaps in demand and supply.</p>
          </div>
          <!-- end col-6 -->
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h4>Implementation</h4>
            <p>Implement your product in India.</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12"> 
            <h4>Sales & Distribution channels</h4>
            <p>Know in details the distribution channels.</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12"> 
            <h4>Location Advantage</h4>
            <p>Understand the advantage of different locations.</p>
          </div>
					<!-- end col-6 -->
					<div class="col-md-4 col-sm-6 col-xs-12">
            <h4>Business Culture</h4>
            <p>Know India work culture.</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12"> 
            <h4>Strategy formulation & Business plan</h4>
            <p>Formulate the strategy to enter the Indian market.</p>
          </div>
          <!-- end col-4 -->
          <div class="col-md-4 col-sm-6 col-xs-12"> 
            <h4>Potential Customers/Business leads/Partners</h4>
            <p>Know your customers.</p>
          </div>
          <!-- end col-6 -->
        </div>
        <!-- end row inner --> 
      </div>
      <!-- end col-8 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<section class="testimonials">
  <div class="container">
    <div class="row">
     <div class="col-xs-12 text-center">
     	<h6>HAPPY CLIENTS</h6>
     	<h2>Testimonials from our clients</h2>
     </div>
     
      <div class="col-xs-12" >
       <div class="owl-slider">
       <div><div class="testimonial-box" style="background: #fff;">
          <figure class="head"> <img src="images/SandrineLECOINTE.jpg" alt="Image"> </figure>
          <h5>Madagas'Care Cosmétiques</h5>
          <small>Sandrine LECOINTE(CEO)</small> <span class="rates"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </span>
          <p>Delighted that Indian students are to study my business development. This allowed me to see the commercial potential of India and appreciate this market that I did not know.
The work of five teams was of good quality and very professional: Teams involved, Successful records, appropriate responses and precise. It is an experience that I recommend!</p>
        </div>
        </div>
        
       <div><div class="testimonial-box" style="background: #fff;">
          <figure class="head"> <img src="images/DidierBaptiste.jpg" alt="Image"> </figure>
          <h5>S&B DIGITAL</h5>
          <small>Didier Baptiste(Founder and CEO)</small> <span class="rates"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </span>
          <p>The reports have shown a deeper analysis of the product, and was highly appreciated by the board. It was "a very mature approach and hopeful analysis for our product."
.</p>
        </div>
        </div>
        
        
       </div>
        
      </div>
      
    </div>
    
  </div>
 
</section>
<section class="support-bar transition">
  <div class="container">
    <div class="row">
      <div class="col-xs-12"> <a href="contact">GET IN TOUCH</a> <img src="images/icon-support.png" alt="Image">
        <h4>Have any Question?</h4>
        <h6>We're here to help.</h6>
      </div>
      <!-- end col-12 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<!-- end work-us 


<section class="highlights">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h6>Benefit for your business</h6>
        <h2>Indian Insiders' mission is to showcase global companies the potential of Indian market and pool of vast opportunities.</h2>
      </div>
 
      <div class="col-md-2 col-md-offset-3 col-sm-4 col-xs-12"> <img src="images/icon1.png" alt="Image">
        <h4>GET INSIGHTS</h4>
        <p>We deliver custom tailor made solutions by analyzing your product in India. Our task is to make you understand Indian market for your product.</p>
      </div>

      <div class="col-md-2 col-sm-4 col-xs-12"> <img src="images/icon2.png" alt="Image">
        <h4>PREVIEW CANDIDTES</h4>
        <p>Our consultants are top Business school students. Get a chance to work with your future employees on live projects. We simplify your hiring process in India.</p>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-12"> <img src="images/icon3.png" alt="Image">
        <h4>GROW YOUR BUSINESS</h4>
        <p>Harness the potential of the huge Indian market and grab the opportunity. Indian is developing fast; grow your business fast with India.</p>
      </div>
    </div>
  </div>
  <!-- end container 
</section>-->


<!--<section class="support-bar transition">
  <div class="container">
    <div class="row">
      <div class="col-xs-12"> <a href="contact">GET IN TOUCH</a> <img src="images/icon-support.png" alt="Image">
        <h4>Have any Question?</h4>
        <h6>We're here to help.</h6>
      </div>
      
    </div>
   
  </div>
 
</section>-->
<!-- end support-bar -->
<footer class="footer transition">
  <div class="footer-bar">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          <ul style="display: block;margin: 0px auto;width: 183px;" class="social-media">
			 		<li><a target="_blank" href="https://www.facebook.com/indianinsiders"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				  <li><a target="_blank" href="https://twitter.com/indianinsiders"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				  <li><a target="_blank" href="https://www.linkedin.com/company/27148513/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          </ul>
        
        </div>
		 <div class="col-md-3 col-sm-6 col-xs-12">
          <address>
          <i class="fa fa-phone" aria-hidden="true"></i> +33 601 86 78 38 </br> +91 95 45 54 00 00</br><small>Any questions? Call us.</small>
          </address>
        </div>
		<div class="col-md-3 col-sm-6 col-xs-12">
          <address>
          <i class="fa fa-map-marker" aria-hidden="true"></i> Paris, France & Pune, India
          </address>
        </div>
      </div>
   
    </div>
 
  </div>
  <!-- end footer-bar -->
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="text-center col-xs-12">Copyright © 2018, Indian Insiders</div>
        <!-- end col-6 -->
       
        <!-- end col-6 --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </div>
  <!-- end sub-footer --> 
</footer>
<!-- end footer --> 
<a href="#" class="scrollup"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a> 

<!-- JS FILES --> 
<script src="js/jquery.min.js"></script> 
<script type="text/javascript">
$(window).load(function() {
	$(".preloader").fadeOut("fast");
})
</script> 
<script src="js/analytics.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jPushMenu.js"></script> 
<script src="js/odometer.min.js"></script> 
<script src="js/owl.carousel.min.js"></script> 
<script src="js/jquery.fancybox.js"></script> 
<script src="js/wow.min.js"></script> 
<script src="js/masonry.pkgd.min.js"></script> 
<script src="js/scripts.js"></script>

<!-- REVOLUTION JS FILES --> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script> 
<script type='text/javascript' src='revolution/js/revolution.addon.slicey.min.js?ver=1.0.0'></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script> 

<script type="text/javascript">
		
			var tpj=jQuery;
			var revapi24;
			tpj(document).ready(function() {
				if(tpj("#rev_slider_24_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_24_1");
				}else{
					revapi24 = tpj("#rev_slider_24_1").show().revolution({
						sliderType:"standard",
						jsFileLocation:"revolution/js/",
						sliderLayout:"fullscreen",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
 							mouseScrollReverse:"default",
							onHoverStop:"off",
							bullets: {
								enable:true,
								hide_onmobile:false,
								style:"bullet-bar",
								hide_onleave:false,
								direction:"horizontal",
								h_align:"center",   
								v_align:"bottom",
								h_offset:0,
								v_offset:50,
								space:5,
								tmp:''
							}
						},
						responsiveLevels:[1240,1024,778,480],
						visibilityLevels:[1240,1024,778,480],
						gridwidth:[1240,1024,778,480],
						gridheight:[868,768,960,720],
						lazyType:"none",
						shadow:0,
						spinner:"off",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						fullScreenAutoWidth:"off",
						fullScreenAlignForce:"off",
						fullScreenOffsetContainer: "",
						fullScreenOffset: "0",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}

                if(revapi24) revapi24.revSliderSlicey();
				
				$("#slide-67-layer-7").click(function(){
					 $("html, body").animate({ scrollTop: $(".header").height() }, 1000);
				});
			});	/*ready*/
		</script>
</body>
</html>