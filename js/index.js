 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var web = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
var phone = /^[0-9,+,-]+$/;;
		$(window).resize(function(){
			setformheight();
			$(".divcontainer").css("height",$(window).height()+"px");
				if($(window).width() > 697){
					$("#divmessageimage").show();
					$(".messageimagebox1").hide();
					$("#divmessageimage div").css("left","-165px");
				}else{
					$("#divmessageimage").hide();
					$(".messageimagebox1").show();
					
				}	
		});
		
		function setformheight(){
			if($(window).height() < 400){
				$(".container").css("margin","10px auto");
			}else{
				$(".container").css("margin","5em auto");
			}
		}	
		
		$(document).ready(function(){
			setformheight();
			$(".divcontainer").css("height",$(window).height()+"px");
			
			$("#next-page").click(function(){
				$('.questions li:first-of-type').slideUp(500, function(){
					$(this).remove();
					$('.questions li:first-of-type .inputextbox').focus();
				});
				$("#next-page").animate({
					opacity: 0
				}, 0);
				submitdata();
			});
			
			$("#cnext-page").click(function(){	
				$('.questions li:first-of-type').slideUp(500, function(){
					$(this).remove();
					$('.questions li:first-of-type .inputextbox').focus();
				});
				$("#cnext-page").animate({
					opacity: 0
				}, 0);
				submitcompanydata();
			});
			
			if($(window).width() > 697){
				$(".messageimagebox1").hide();
				  setTimeout(function(){ 
						$("#divmessageimage").css({
							"opacity":"0",
							"display":"block",
						}).show().animate({opacity:1});
						$("#divmessageimage div").css("left","-165px");
				 }, 1000);
			}else{
				 setTimeout(function(){ 
				$(".messageimagebox1").show();
				}, 3000);
			}
			 setTimeout(function(){ 
					$("#divmessage1").css({
						"opacity":"0",
						"display":"block",
					}).show().animate({opacity:1});
					showmessage2();
			 }, 3000);
			 $("#hiddenyear").val($('input[name=collegeyear]:checked', '#radioyear').val()); 
			 $('#radioyear input').on('change', function() {
			   $("#hiddenyear").val($('input[name=collegeyear]:checked', '#radioyear').val()); 
			});
			
		});
		
		function showmessage2(){
			setTimeout(function(){ 
					$("#divmessage2").css({
						"opacity":"0",
						"display":"block",
					}).show().animate({opacity:1});
					$("#divmessage1").removeClass("speech-bubble");
					showmessage3();
			 }, 2000);
		}
		
		function showmessage3(){
			setTimeout(function(){ 
					$("#divmessage3").css({
						"opacity":"0",
						"display":"block",
					}).show().animate({opacity:1});
					$("#divmessage2").removeClass("speech-bubble2");
			 }, 2000);
		}
		function shownextbutton(controlid){
			flag = false;
			$(".error-message").html("");		
			var controlvalue = $("#"+controlid).val().trim();
			if(controlvalue != null && controlvalue != ""){
				if(controlid == "txtcemail"){
					 if(controlvalue.match(re)){
						$("#errormessage").html("");
						$("#errormessage").hide(100);
						$(".container1").remove();
						 $(".container").css({
							"opacity":"0",
							"display":"inline-block",
						}).show().animate({opacity:1});
						$("#hiddenemail").val(controlvalue);
						submitdata();
						$('.questions li:first-of-type input').focus();
					 }else{ 
						$("#errormessage").html("Please enter valid email id.");
						$("#errormessage").show(100);
						flag = false;
					 }
				}else{
					 if(!controlvalue.match(re)){
						 if(controlid == "txtuname"){
							 $("#hiddenuname").val(controlvalue);
							 flag = true;
						 }else if(controlid == "txtluname"){
							  $("#hiddenlastuname").val(controlvalue);
							  flag = true;
						 }
						 else if(controlid == "txtcname"){
							 $("#hiddencname").val(controlvalue);
							 flag = true;
						 }else if(controlid == "txtstream"){
							 $("#hiddenstream").val(controlvalue);
							 flag = true;
						 }
					 }else{
						 $("#next-page").animate({
							opacity: 0
						}, 0);
						$("#next-page").hide();
					 }
				}
				
			}else{
				if(controlid == "txtcemail"){
					$("#errormessage").html("Please enter valid email id.");
					$("#errormessage").show(100);
				}
				
				$("#next-page").animate({
					opacity: 0
				}, 0);
				$("#next-page").hide();
				$(".error-message").html("Please fill blank field");
			}
			
			if(flag){
				$("#next-page").show();
				$("#next-page").animate({
					opacity: 1
				}, 100);
				
			}else{
				$("#next-page").hide();
				$("#next-page").animate({
					opacity: 0
				}, 0);
			}
			return flag;
		}
		
		
		function hidelables(){
			$("#errormessage").hide(100);
		}
		
		function submitdata(){
			var emailid =  $("#hiddenemail").val();
			var name =  $("#hiddenuname").val();
			var lastname =  $("#hiddenlastuname").val();
			var collegename =  $("#hiddencname").val();
			var stream =  $("#hiddenstream").val();
			var year = $("#hiddenyear").val();
			var userid = $("#hiddenuserid").val();
			var sendInfo = [name, emailid, collegename, stream, year, userid, lastname];
			$.post("ajax/userdetails.php", { formdata: sendInfo }, function (e) {
				if(e > 0){
					$("#hiddenuserid").val(e);
				}	
			});
		}
		
/***************************************company***************************************************************/
		
		function submitcompanydetails(controlid){
			var flag = false;
			$(".error-message").html("");			
			var controlvalue = $("#"+controlid).val().trim();
			$("#hidden"+controlid).val(controlvalue);
			if(controlvalue != null && controlvalue != ""){
				if(controlid == 'txtcompanyemail'){
					if(controlvalue.match(re)){
						$("#errormessage").html("");
						$("#errormessage").hide(100);
						$(".container1").remove();
						$(".container").css({
							"opacity":"0",
							"display":"inline-block",
						}).show().animate({opacity:1});
						submitcompanydata();
						$("#cnext-page").hide();
						$("#cnext-page").animate({
							opacity: 0
						}, 0);
						$('.questions li:first-of-type input').focus();
					}else{
						flag = false;
						$("#errormessage").html("Please enter valid email id.");
						$("#errormessage").show(100);
					}
				}else if(controlid == "txtcfuname" || controlid == "txtcluname" || controlid == "txtclocation" || controlid == "txtcindustry" || controlid == "txtmessage"){
					if(!controlvalue.match(re)){
						flag = true;
					}else{
						flag = false;
						$(".error-message").html("Please enter valid value");	
					}
				}
				
				if(controlid == "txtphonenumber"){
					if(controlvalue.match(phone)){
						flag = true;
					}else{
						flag = false;
						$(".error-message").html("Please enter valid phone number.");
					}
				}else if(controlid == "txtcwebsite"){
					if(controlvalue.match(web)){
						flag = true;
					}else{
						flag = false;
						$(".error-message").html("Please enter valid url.");
					}
				}
			}else{
				flag = false;
				if(controlid == 'txtcompanyemail'){
					$("#errormessage").html("Please enter valid email id.");
					$("#errormessage").show(100);
				}else{
					$("#cnext-page").hide();
					$("#cnext-page").animate({
						opacity: 0
					}, 0);
				}
				$(".error-message").html("Please fill blank field");
			}
			
			if(flag){
				$("#cnext-page").show();
				$("#cnext-page").animate({
					opacity: 1
				}, 100);
				
			}else{
				$("#cnext-page").hide();
				$("#cnext-page").animate({
					opacity: 0
				}, 0);
				
			}
			
			return flag;
		}
		
		function submitcompanydata(){
			var emailid =  $("#hiddentxtcompanyemail").val();
			var fname =  $("#hiddentxtcfuname").val();
			var lname =  $("#hiddentxtcluname").val();
			var location =  $("#hiddentxtclocation").val();
			var industry =  $("#hiddentxtcindustry").val();
			var message =  $("#hiddentxtmessage").val();
			var phone = $("#hiddentxtphonenumber").val();
			var website = $("#hiddentxtcwebsite").val();
			var hiddenuserid = $("#hiddencuserid").val();
			var sendInfo = [fname, emailid, location, industry, message, phone, website, lname, hiddenuserid];
			$.post("ajax/userdetails.php", { companyformdata: sendInfo }, function (e) {
				if(e > 0){
					$("#hiddencuserid").val(e);
				}	
			});
		}
		