<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="My Resume Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<title>Indian Insiders</title>

	<meta property="og:url"           content="http://www.indianinsiders.com/students" />
	<meta property="og:type"          content="Consulting" />
	<meta property="og:title"         content="Indian Insiders" />
	<meta property="og:description"   content="Are you ready to be a globlal consultant. while being a student?" />
	<meta property="og:image"         content="images/Logo1000X1000.png" />
	
	<!-- TWITTER META -->
	<meta name="twitter:site" content="http://www.indianinsiders.com/students">
	<meta name="twitter:creator" content="http://www.indianinsiders.com/students">
	<meta name="twitter:title" content="Are you ready to be a globlal consultant. while being a student?Join http://www.indianinsiders.com/students #IndianInsiders">
	<meta name="twitter:description" content="Are you ready to be a globlal consultant. while being a student?Join http://www.indianinsiders.com/students #IndianInsiders">
	<meta name="twitter:image" content="images/Logo1000X1000.png">
	
	<!-- FAVICON FILES -->
<link href="images/favicon.png" rel="apple-touch-icon" sizes="144x144">
<link href="images/favicon.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/favicon.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/favicon.png" rel="apple-touch-icon">
<link href="images/favicon.png" rel="shortcut icon">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900" rel="stylesheet">	
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<title></title>
<!--mobile apps-->
<!--Custom Theme files-->
	<!--<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">-->
    <link rel="stylesheet" href="css/style1.css">
	<script src="js/jquery.min.js"></script> 
</head>

<body>
	<input type="hidden" id="hiddenemail" name="hiddenemail" value="">
	<input type="hidden" id="hiddenuname" name="hiddenuname" value="">
	<input type="hidden" id="hiddenlastuname" name="hiddenlastuname" value="">
	<input type="hidden" id="hiddencname" name="hiddencname" value="">
	<input type="hidden" id="hiddenstream" name="hiddenstream" value="">
	<input type="hidden" id="hiddenyear" name="hiddenyear" value="">
	<input type="hidden" id="hiddenuserid" name="hiddenuserid" value="">
	<div class="companylogo"><a href="http://www.indianinsiders.com"><img src="images/ilogoyellow.png"></a></div>
	<div class="divcontainer" style="overflow-y: auto;">
	<div class="container1">
		<div class="container1div">	
		<div class="messageimagebox1">
			<div>
				<img height="100" src="images/imagespic.png" width="100">
			</div>
		</div>
		<div id="divmessage1" style="display:none;" class="messagebox speech-bubble">
			Hi I am Anurag - CEO and Founder of Indian Insiders, an Indo French Company that helps business in Europe and USA to understand Indian Market
		</div>	
		<div id="divmessage2" style="display:none;" class="messagebox speech-bubble2">
			We provide MBA student opportunity to consult companies and analyze their business for Indian market and you can be a part of it
			<img width="100%" src="images/iigif.gif" loop=infinite/>
		</div>	
		<div id="divmessage3" style="display:none;" class="messagebox speech-bubble">
			Get early access to our exclusive program by registering
			</br>
			<input type="text" oninput="hidelables()" class="textboxforemail" id="txtcemail" name="txtcemail" placeholder="Enter college email" autofocus/>
			<button onclick="shownextbutton('txtcemail')" class="guestSubmit_58c33 submit_03c36" id="buttonsubmitemail" type="submit"><span><svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1600 960q0 54-37 91l-651 651q-39 37-91 37-51 0-90-37l-75-75q-38-38-38-91t38-91l293-293h-704q-52 0-84.5-37.5t-32.5-90.5v-128q0-53 32.5-90.5t84.5-37.5h704l-293-294q-38-36-38-90t38-90l75-75q38-38 90-38 53 0 91 38l651 651q37 35 37 90z" fill="#fff"></path></svg></span></button>
		</div>
		<span id="errormessage" style="display:none;" class="errormessage"></span>
		<div style="display:none;" id="divmessageimage" class="messageimagebox">
			<div>
				<img height="100" src="images/imagespic.png" width="100">
			</div>
		</div>
		</div>
	</div>
		
     <div style="display:none;" class="container">
            <form id="sign-form" class="sign-form">
                <ol class="questions">
                    <li>
				        <span><label for="uname">What is your first name?</label></span>
                        <input class="inputextbox" id="txtuname" oninput="shownextbutton('txtuname')" name="txtuname" type="text" value="" placeholder="Enter your first name" autofocus/>
					</li>
					<li>
				        <span><label for="uname">What is your last name?</label></span>
                        <input class="inputextbox" id="txtluname" oninput="shownextbutton('txtluname')" name="txtluname" type="text" value="" placeholder="Enter your last name" autofocus/>
					</li>
                    <li>
				        <span><label for="cname">Enter you college name?</label></span>
                        <input class="inputextbox" id="txtcname" oninput="shownextbutton('txtcname')" name="txtcname" type="text" value="" placeholder="Enter college name"  autofocus/>
					</li>
                    <li>
                        <span><label for="streamandyear">MBA Specialization</label></span>
                        <input id="txtstream" name="txtstream" oninput="shownextbutton('txtstream')" type="text" value="" placeholder="Enter your MBA Specialization" class="inputextbox" autofocus/>
						<div id="radioyear" class="responsivfont"><input style="width:unset;" type="radio" name="collegeyear" value="1" checked> 1<sup>st</sup>year
						<input style="width:unset;" type="radio" name="collegeyear" value="2"> 2<sup>nd</sup>year</div>
						
					</li>
                    <li class="thankyoumessagebox" style="text-align:center;"><p>
                        <span href="#" style="color:#000;text-decoration:none">Thank you and we will get back to you soon.</span></p>
						<div style="margin-top: 15px;">
						<a style="text-decoration:none;"  target="_blank" href="https://twitter.com/intent/tweet?text=Are you ready to be a globlal consultant. while being a student?Join http://www.indianinsiders.com/students #IndianInsiders" class="sharebtncss twitter"><span style="display:inline"><svg width="16" height="13" viewBox="0 0 16 13" xmlns="http://www.w3.org/2000/svg"><path d="M15.999,1.5367041 C15.4105184,1.79765391 14.7775382,1.97411998 14.1135589,2.05360469 C14.7910377,1.64718285 15.3115215,1.00430648 15.5570138,0.237953855 C14.9225336,0.613881561 14.2200556,0.887328975 13.472579,1.03430071 C12.8735977,0.39642338 12.0206243,-0.002 11.0766538,-0.002 C9.26371048,-0.002 7.7942564,1.46721746 7.7942564,3.27986887 C7.7942564,3.53731936 7.82325549,3.7877712 7.87925374,4.02772505 C5.15133899,3.89075139 2.73241458,2.58400269 1.11346517,0.598384541 C0.830974001,1.08329129 0.668979063,1.64668295 0.668979063,2.2485672 C0.668979063,3.3873482 1.24846095,4.39165507 2.12943342,4.98054182 C1.59145024,4.96354509 1.08546605,4.81607345 0.642479891,4.57012075 C0.641979907,4.58361815 0.641979907,4.59761546 0.641979907,4.61161277 C0.641979907,6.20180696 1.77344455,7.52805191 3.27489763,7.82949394 C2.99940624,7.90447952 2.7094153,7.94447183 2.40992466,7.94447183 C2.19843127,7.94447183 1.99293769,7.92397577 1.79244395,7.88548318 C2.20993091,9.18923246 3.42239302,10.13805 4.85884813,10.1645449 C3.73538324,11.0448756 2.31992747,11.5692748 0.781975532,11.5692748 C0.516983813,11.5692748 0.255991969,11.5537777 -0.001,11.5232836 C1.45145461,12.4546045 3.17690069,12.998 5.03084275,12.998 C11.0686541,12.998 14.3700509,7.99696174 14.3700509,3.65979581 C14.3700509,3.51732321 14.367051,3.37585041 14.3605512,3.23537743 C15.0020312,2.77246645 15.5585138,2.19457758 15.9985,1.5367041 L15.999,1.5367041 Z" fill="#FFF"></path></svg></span><span style="display:inline;font-size:18px;">Tweet</span></a>
						<a style="text-decoration:none;" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.indianinsiders.com%2Fstudents&layout=button_count&size=small&mobile_iframe=true&appId=1596610930363917&width=1000&height=1000" class="sharebtncss facebook"><span style="display:inline"><svg width="8" height="13" viewBox="0 0 8 14" xmlns="http://www.w3.org/2000/svg"><path d="M7.2 2.323H5.923c-1.046 0-1.278.464-1.278 1.16V5.11h2.44l-.35 2.438h-2.09v6.387H2.09V7.548H0V5.11h2.09V3.252C2.09 1.162 3.368 0 5.342 0c.93 0 1.742.116 1.858.116v2.207z" fill="#FFF" fill-rule="evenodd"></path></svg></span><span style="display:inline;font-size:18px;">Share</span></a>
						</div>      
                </ol>
                <div id="next-page" alt="Kiwi standing on oval"></div>
                <div style="margin-top: 24px;" class="error-message"></div>
                
            </form>
        <h1 id="wf" style="opacity:0; margin-top:1.1em;display:none; margin-bottom:1em">Thank you</h1>
	</div>
</div>	
<script src="js/index.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
	$(document).keypress(function(e) {
		if(e.which == 13) {
			if($(".container1").length == 1){
				shownextbutton('txtcemail');
			}else{
				$("#sign-form").submit(function(e) {
					e.preventDefault();
				});
				var inputid = $('.questions li:first-of-type input').attr('id');
				if(shownextbutton(inputid)){
					$("#next-page").click();
					$(".error-message").html("");
				}
			}
		}
	});
});	
</script>
</body>
</html>
