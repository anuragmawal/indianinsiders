<nav class="navbar navbar-default navbar-fixed-top transition">
  <!-- end top-bar -->
    <div class="container">
      <div class="navbar-header">
        <!--<button type="button" class="navbar-toggle toggle-menu menu-left push-body" data-toggle="collapse" data-target="#collapse-nav"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>-->
        <a class="navbar-brand" href="index"> <img src="images/logo-light.png" alt="Image" class="logo-light"> <img src="images/logo-dark.png" alt="Image" class="logo-dark"></a>
		<ul class="nav navbar-nav visible-xs hidden-sm">
			<!--<li class="mbastudentslink"><a style="font-size: 0.8em;" target="_blank" href="contact">Contct</a></li>
			<li class="mbastudentslink"><a style="font-size: 0.8em;" target="_blank" href="blog.html">Blog</a></li>-->
			<li class="mbastudentslink"><a style="font-size: 0.8em;" target="_blank" href="students">MBA Students</a></li>
		</ul>	
		</div>
      <div class="collapse navbar-collapse cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="collapse-nav">
        <ul class="top-menu visible-xs">
		  <li><a href="our-offices.html">Our Offices</a></li>
		  <li><a href="career.html">Career</a></li>
		  <li><a href="on-press.html">On Press</a></li>
		  <li><a href="support.html">Support</a></li>
		</ul>
            <!-- end top-menu --> 
        <ul class="nav navbar-nav">
          <li style="visibility:hidden;"><a href="index.html">HOME</a></li>
          <li style="visibility:hidden;" class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">PAGES</a>
            <ul class="dropdown-menu">
              <li><a href="about-us.html">About Us</a></li>
              <li><a href="solutions.html">Solutions</a></li>
              <li><a href="partners.html">Partners</a></li>
              <li><a href="gallery.html">Gallery</a></li>
              <li><a href="pricing.html">Pricing</a></li>
              <li><a href="faq.html">FAQ</a></li>
              <li><a href="elements.html">Elements</a></li>
              <li><a href="404.html">404 Page</a></li>
            </ul>
          </li>
          <li style="display:none;"><a href="projects.html">PROJECTS</a></li>
         <!-- <li><a target="_blank" href="students">MBA Students</a></li>-->
		  <!--<li><a href="blog.html">Blog</a></li>
          <li><a href="contact">Contact</a></li>-->
        </ul>
        <!-- end navbar-nav --> 
         <a href="contact" class="quote-btn">CONTACT US</a>
        <span class="phone"><i class="fa fa-phone" aria-hidden="true"></i> +33 601 86 78 38 / +91 95 45 54 00 00</span> 
        <!-- end phone --> 
        <!--<div class="search"><i class="fa fa-search" aria-hidden="true"></i></div>-->
        <!-- end search -->
      </div>
      <!-- end navbar-collapse --> 
    </div>
    <!-- end container --> 
  </nav>