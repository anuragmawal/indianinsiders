
var express = require('express');
var app = express();
var fs = require('fs');
var http = require("http").Server(app).listen(3000,function(){
    console.log('server is runing live.');
});
var upload = require("express-fileupload");
var session = require('express-session');
var imagdir = '../upload/profilepic/';
app.use(upload());
var bodyParser = require("body-parser");
var randomstring = require("randomstring");

var nodemailer = require('nodemailer');
//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(session({secret:"f4566g346f546f",resave:false,saveUninitialized:true}));
app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
  
//database connection 
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
//var databasename = 'indianinsiderdemo1';
var databasename = 'indianinsidertest1';

var moment = require('moment');
app.locals.moment = require('moment');
/*
app.listen(3000,function(){
    console.log('server is runing live.');
});
*/
app.get('/',function(req, res) {
    res.render('index');  
   // res.sendFile(__dirname+'/index.html'); 
});

app.get('/index',function(req, res) {
    res.render('index');  
   // res.sendFile(__dirname+'/index.html'); 
});

app.get('/contact',function(req, res) {
    res.render('contact');  
});

app.get('/about',function(req, res) {
    res.sendFile(__dirname+'/about.html');
});
/*
app.get('/contact',function(req, res) {
    res.send("this is contact page"); 
})
*/
var students = {
    1:{
        name:"pravin",
        subject :['c++','java','php']
    },
    2:{
        name : 'hites',
        subject :['angular','node','html']
    },
    3:{
        name : 'ritesh',
        subject :['css','javascript','jq']
    },
}
/*
app.get('/student/:id',function(req, res) {
    res.send("student name :"+ students[req.params.id]); 
})
*/

app.get('/students',function(req, res) {
   // res.render('students',{name : students[req.params.id].name, id : req.params.id, subjects : students[req.params.id].subject}); 
   res.render('students');
});


app.get('/profile/:id',function(req, res) {
    try{
       if(ObjectId.isValid(req.params.id)){
            //console.log("session id "+req.session.email);
            if(req.session.profileregId === req.params.id){ 
                MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                if(err){
                    console.log(err);
                }else{
                    var dbo = db.db(databasename);
                    var objectid = {_id : ObjectId(req.params.id)};
                    dbo.collection('students').findOne(objectid,function(err, data){
                        //console.log("profile pdf show : "+data.details.basicDetails.firstName);
                        var imgurl = "";
                        if(data.profilPic){
                            imgurl = imagdir+""+data.profilPic;
                        }
                        console.log("profile pic : "+imgurl);
                        var pdfurl = "";
                        var fname = data.details.basicDetails.firstName;
                        var lname = data.details.basicDetails.lastName;
                        var emailadd = data.emailId;
                        var collegname = data.details.educationalDetails.postgGrauationCollege; 
                        var applycasestudies = data.details.casestudies 
                        var userid = data._id;
                        var graduationstream = data.details.educationalDetails.postgGrauationStream;
                        var graduationyear = data.details.educationalDetails.postgGrauationYear;
                        var linkedinprofile = data.details.basicDetails.linkedin;
                        var usercontact =  data.details.basicDetails.phone;
                        if(data.resumePdf){
                        pdfurl = data.resumePdf; 
                        }
                        if(err){
                            console.log(err);
                        }else{
                            dbo.collection('tblcompanyinfo').find({}).toArray(function(err, resdb){
                                if(err){
                                    console.log(err);
                                }else{   
                                //console.log("company name : "+resdb.companyName);
            
                                res.render('profile',{userfname : fname, userlname : lname, contcact : usercontact, linkedinurl : linkedinprofile, postgraduationstream : graduationstream,postgratuationyear : graduationyear, profileid : userid, userprofilepic : imgurl, userpdfurl : pdfurl, username : fname+" "+lname, useremail : emailadd, usercollege : collegname ,casestudy : resdb, appliedcasestudy :applycasestudies});
                                /* res.render('profile',{cmpname : resdb.name, cmpdes : resdb.description, cmpurl : resdb.url,
                                    cmpstatics : resdb.statistics, cmpcurrentmarket : resdb.currentMarket, cmpbackgroundproblem : resdb.caseStudy.BackgroundProblem,
                                cmpgenralproblem : resdb.caseStudy.GeneralProblem, cmpspecificproblem : resdb.caseStudy.specificProblem,
                                cmpcasecategary : resdb.caseStudy.GeneralProblem, cmpanyspecific : resdb.caseStudy.anySpecificReq,
                            cmpcompanies : resdb.caseStudy.companies, cpmcandidates : resdb.caseStudy.candidates}); */
                                }
                            });
                            db.close();
                            
                            }
                            
                        });

                
                    }
                });

            }else{
                res.redirect("/login");
            }
        }else{
            res.send(false);
        }
    }catch(err){
        console.log(err);
    } 
});  

app.post('/login',function(req, res) {
    try{
        var usermailid =  req.body.txtemailid;
        var userpassword = req.body.txtpassword;
        if(usermailid == "admin" && userpassword == "admin"){
            req.session.admin = "admin";
            res.send("/admin"); 
        }else{
            MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                if(err){
                    console.log(err);
                }else{
                    var dbo = db.db(databasename);
                    dbo.collection('students').findOne({emailId:usermailid, password:userpassword},function(err, data){
                        if(err){
                            console.log(err);
                        }else{
                            if(data !== null){
                                console.log(data.details.casestudies);
                                req.session.profileregId = data._id; 
                                res.send("/profile/"+data._id);
                            }else{ 
                                dbo.collection('tblcompanyinfo').findOne({companyemail:usermailid, password:userpassword},function(err, data){
                                    if(err){
                                        console.log(err);
                                    }else{
                                        if(data !== null){
                                            req.session.company = data._id; 
                                            res.send("/companyinfo/"+data._id);
                                        }else{
                                            res.send(false);   
                                        }
                                    }
                                db.close();
                                });    
                            }
                        }
                    db.close();
                    }); 
                }
            });
        }
    }catch(err){
        console.log(err);
    }
});

app.post('/profile',function(req, res) {
    try{
        if(req.body.suggest){
            //res.send(req.body.suggest);
            var img = req.body.suggest;
            // strip off the data: url prefix to get just the base64-encoded bytes
            var data = img.replace(/^data:image\/\w+;base64,/, "");
            var buf = new Buffer(data, 'base64');
            var val = Math.floor(1000 + Math.random() * 9000);
            var imagename = val+'image.png';
            fs.writeFile(__dirname+'/public/upload/profilepic/'+imagename, buf);
            MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                    if(err){
                        console.log(err);
                    }else{
                        console.log("connected");
                        var dbo = db.db(databasename);
                        //console.log("get id "+req.session.profileregId);
                        var oldvalue = {_id : ObjectId(req.session.profileregId)};
                        var newvalue = {$set: {profilPic : imagename}}
                        dbo.collection('students').updateMany(oldvalue, newvalue, function(err, data){
                            if(err){
                                console.log(err);
                            }else{
                                console.log('%d docs update',data.result.nModified);
                                res.send(imagename);
                            }
                             db.close();
                        });
                       
                       
                    }
                });
        }
    }catch(err){
        console.log(err);
    }
 
});

app.post("/uploadfiles",function(req, res){
    try{
        if(req.files && req.session.profileregId){
            var caseid = req.body.inptucasestudy;
            var commenttextbox = req.body.txtcomment;
            var file = req.files.filename,
            filename = file.name;
            if(filename.split('.').pop() === 'pdf'){
                file.mv("./public/upload/pdffiles/"+filename,function(err){
                    if(err){
                        console.log(err);
                    }else{
                        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                            if(err){
                                console.log(err);
                            }else{
                                console.log(caseid +" "+filename);
                                var uploadcasestudy = { "id":caseid, "comment": commenttextbox,"pdf": filename};
                                var dbo = db.db(databasename);
                                var oldvalue = {_id : ObjectId(req.session.profileregId)};
                                var newvalue = {$push: {'details.casestudies' : uploadcasestudy}};
                                dbo.collection('students').updateMany(oldvalue, newvalue,function(err, data1){
                                    if(err){
                                        console.log(err);
                                    }else{
                                    
                                        dbo.collection('tblcompanyinfo').updateMany({ _id : ObjectId(caseid)}, {$push: {'applieduser': {'userid' : ObjectId(req.session.profileregId)}}},function(err, data2){
                                            if(err){
                                                console.log(err);
                                            }else{
                                                console.log('%d docs update',data2.result.nModified);
                                                res.redirect('/compnayinfopdfpriew/'+caseid);
                                            }
                                            db.close();
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }else{
                res.redirect('/companyinfo/'+caseid);
            }
        }else{
            res.redirect('/login');
        }
    }catch(err){
        console.log(err);
    }
});

app.get('/download/:id',function(req, res){
    try{
        if(ObjectId.isValid(req.params.id)){
            var userid = req.params.id;
            MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                if(err){
                    console.log(err);
                }else{
                    var dbo = db.db(databasename);
                    var objectid = {_id : ObjectId(userid)};
                    dbo.collection('students').findOne(objectid,function(err, data){
                        var pdfurl = data.resumePdf;
                        if(err){
                            console.log(err);
                        }else{
                        // res.render('profile',{userprofilepic : imgurl, userpdfurl : pdfurl});
                            res.download(__dirname+"/public/upload/resume/"+pdfurl, pdfurl);
                        }
                            db.close();
                    });
                }
            });
        }else{
            res.send(false);
        }
    }catch(err){
        console.log(err);
    }    
});


app.get('/downloadanswer/:id/:casid',function(req, res){
    try{
        if(ObjectId.isValid(req.params.id) && ObjectId.isValid(req.params.casid)){
            var userid = req.params.id;
            var caseid = req.params.casid;
            MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                if(err){
                    console.log(err);
                }else{
                    var dbo = db.db(databasename);
                    var objectid = {_id : ObjectId(userid)};
                    dbo.collection('students').findOne(objectid,function(err, data){
                        //var pdfurl = data.resumePdf;
                        if(err){
                            console.log(err);
                        }else{
                            for(var i=0; i<data.details.casestudies.length;i++){
                                if(caseid == data.details.casestudies[i].id){
                                res.download(__dirname+"/public/upload/pdffiles/"+data.details.casestudies[i].pdf, data.details.casestudies[i].pdf);
                                }
                                
                            }
                        }
                            db.close();
                    });
                }
            });
        }else{
            res.send(false);
        }
    }catch(err){
        console.log(err);
    }    
});

app.get('/login',function(req, res){
    try{
        req.session.destroy(function(err){  
            if(err){  
                console.log(err);  
            }   
        }); 
        res.render('login'); 
    }catch(err){
        console.log(err);
    }
});

app.get('/picturecut',function(req, res){
    /*console.log(req);
    var img = req.params.imagurl;
    if(img == 1){
    	var img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0"
		+ "NAAAAKElEQVQ4jWNgYGD4Twzu6FhFFGYYNXDUwGFpIAk2E4dHDRw1cDgaCAASFOffhEIO"
		+ "3gAAAABJRU5ErkJggg==";
        }
	// strip off the data: url prefix to get just the base64-encoded bytes
	var data = img.replace(/^data:image\/\w+;base64,/, "");
	var buf = new Buffer(data, 'base64');
	fs.writeFile(__dirname+'/public/upload/profilepic/image.png', buf);*/
});

app.post('/logout',function(req, res){
    try{
        req.session.destroy(function(err){  
            if(err){  
                console.log(err);  
            }  
            else  
            {  
                res.send(true);  
            }  
        });
    }catch(err){
        console.log(err);
    } 
});

app.post('/userdetails',function(postreq, postres){
    try{
        var txtemail = postreq.body.userinfo[1];
        var txtfname = postreq.body.userinfo[0];
        var txtlname = postreq.body.userinfo[5];
        var txtcollegename = postreq.body.userinfo[2];
        var txtstream = postreq.body.userinfo[3];
        var txtyear = postreq.body.userinfo[4];

        var isinserted = false;
        var rec = {
            "emailId":txtemail,
            "password":"",
            "details":{
                "basicDetails": {
                    "firstName":"",
                    "lastName":"",
                    "gender":"",
                    "profilPic":"",
                    "phone":"",
                    "linkedin":""
                },
                "educationalDetails":{
                    "resumePdf": "",
                    "postgGrauationCollege":"",
                    "postgGrauationStream":"",
                    "postgGrauationYear":""
                },
                "casestudies":[],
            }
        };

        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
            var dbo = db.db(databasename);
            if(err){
                console.log(err);
            }else{
                dbo.collection('students').findOne({ "emailId": txtemail},function(err, res){
                    if(err){
                        console.log(err);
                    }else{
                        
                        if(res !== null){
                                    isinserted = true;
                            }

                            if(!isinserted){
                                
                                    dbo.collection('students').insert([rec],function(err, loginres){
                                        if(err){
                                            console.log(err);
                                        }else{
                                            //console.log('%d docs inserted',loginres.insertedCount);
                                            //console.log("Basicdetails :"+ loginres[""])
                                            //console.log(loginres.details);
                                            console.log(loginres["ops"][0]["_id"]);
                                            postreq.session.profileid = loginres["ops"][0]["_id"]; 
                                            postres.send(true);
                                        }
                                        db.close();
                                    });
                                
                            }else if(postreq.session.profileid){
                                    var myquery = {'emailId' : txtemail};    
                                    var newvalues = { $set: {'details.basicDetails.firstName': txtfname, 'details.basicDetails.lastName': txtlname, 
                                    'details.educationalDetails.postgGrauationCollege': txtcollegename, 'details.educationalDetails.postgGrauationStream': txtstream,
                                    'details.educationalDetails.postgGrauationYear': txtyear} };
                                    dbo.collection('students').update(myquery, newvalues,function(err, res){
                                        if(err){
                                            console.log(err);
                                        }else{
                                            console.log(res.result.nModified + " document(s) updated");
                                            postres.send(true);
                                        }
                                        db.close();
                                    }); 
                            }else{
                                postres.send(false);
                            }
                    }
                    db.close();
                });

            
            }
            
        });    
    }catch(err){
        console.log(err);
    }    
});

app.get('/companyinfo',function(req, res){
    try{
        if( req.session.admin == "admin"){
            res.render("companyinfo",{loginfrom:"2"});
        }else{
            res.redirect("/login");
        }  
    }catch(err){
        console.log(err);
    }  
});


app.post('/companyinfo',function(req, res){
    try{
        var companylogo = req.body.txtcomanylogo;
        var data = companylogo.replace(/^data:image\/\w+;base64,/, "");
        var buf = new Buffer(data, 'base64');
        var val = Math.floor(1000 + Math.random() * 9000);
        var imagename = val+'image.png';
        fs.writeFile(__dirname+'/public/upload/companylogo/'+imagename, buf);
        var companypassword = randomstring.generate(7);
        var cinfo = {
            'companylogo': imagename,
            'caseStudyName': req.body.txtcasestudy,
            'creatorId': 'abc',
            'name' : req.body.txtcompanyname,
            'description' : req.body.txtcdescription,
            'url' : req.body.txtcurl,
            'statistics' : req.body.txtcstatistics,
            'currentMarket' : req.body.txtcurrentmarket,
            'dateCreated': new Date(),
            'caseStudy':{
                'BackgroundProblem' : req.body.txtbackgroundproblem,
                'GeneralProblem' : req.body.txtgenralproblem,
                'specificProblem' : req.body.txtspecificproblem,
                'companies' : req.body.txtcompanies,
                'candidates' : req.body.txtcandidates,
                'caseCategary' :   req.body.txtcasecategory,
                'anySpecificReq' : req.body.txtanyspecificsrequirment
            },
            'applieduser':[  
            ],
            'isCaseStudySolved': 0,
            'status' : 1,
            'closedate' : req.body.txtdatepicker,
            'companyemail':req.body.txtcompanyemail,
            'password': companypassword
        }

        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
            var dbo = db.db(databasename);
            dbo.collection('tblcompanyinfo').insert([cinfo],function(err, cmpres){
                console.log("fetch : id "+cmpres["ops"][0]["_id"]);
                //res.redirect('/compnayinfopdfpriew/'+cmpres["ops"][0]["_id"]);
                res.send("/admin");
            });
        });
    }catch(err){
        console.log(err);
    }  
});


app.get('/compnayinfopdfpriew/:id',function(req, res){
    try{
        if(ObjectId.isValid(req.params.id)){
        var loginfrom = "1";
        if(req.session.admin){
            loginfrom = "2";        
        }else if(req.session.company){
            loginfrom ="3";
        }
        if(req.session.admin || req.session.profileregId){
            var _id = new ObjectId(req.params.id);
            console.log("Id : "+_id);
            MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                var dbo = db.db(databasename);
                if(err){
                    console.log(err);
                }else{
                    dbo.collection('tblcompanyinfo').findOne({ "_id": _id},function(err, resdb){
                        if(err){
                            console.log(err);
                        }else{   
                        //console.log("company name : "+resdb.applieduser[0].userid);
                        res.render("compnayinfopdfpriew",{companylogo : resdb.companylogo,Caseid : _id,cmpname : resdb.name, cmpdes : resdb.description, cmpurl : resdb.url,
                            cmpstatics : resdb.statistics, cmpcurrentmarket : resdb.currentMarket, cmpbackgroundproblem : resdb.caseStudy.BackgroundProblem,
                        cmpgenralproblem : resdb.caseStudy.GeneralProblem, cmpspecificproblem : resdb.caseStudy.specificProblem,
                        cmpcasecategary : resdb.caseStudy.caseCategary, cmpanyspecific : resdb.caseStudy.anySpecificReq,
                    cmpcompanies : resdb.caseStudy.companies, cpmcandidates : resdb.caseStudy.candidates, closingdate:resdb.closedate,
                    companyemail:resdb.companyemail,companypassword:resdb.password,loginfrom:loginfrom,casestudyname : resdb.caseStudyName});
                        }
                    });
                
                    db.close();
                }
            });
        }else{
            res.redirect("/login");
        }
        }else{
            res.send(false);
        }
    }catch(err){
        console.log(err);
    }  
});

app.get('/companyinfo/:id',function(req, res){
    try{
        if(ObjectId.isValid(req.params.id)){
        var loginfrom = "1";
        if(req.session.admin){
            loginfrom = "2";        
        }else if(req.session.company){
            loginfrom ="3";
        }
        if(req.session.admin || req.session.company){
            var _id = new ObjectId(req.params.id);
            MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                var dbo = db.db(databasename);
                if(err){
                    console.log(err);
                }else{
                    dbo.collection('tblcompanyinfo').findOne({ "_id": _id},function(err, resdb){
                        if(err){
                            console.log(err);
                        }else{   

                        res.render("companyinfo",{companylogo : resdb.companylogo, Caseid : _id,cmpname : resdb.name, cmpdes : resdb.description, cmpurl : resdb.url,
                            cmpstatics : resdb.statistics, cmpcurrentmarket : resdb.currentMarket, cmpbackgroundproblem : resdb.caseStudy.BackgroundProblem,
                        cmpgenralproblem : resdb.caseStudy.GeneralProblem, cmpspecificproblem : resdb.caseStudy.specificProblem,
                        cmpcasecategary : resdb.caseStudy.caseCategary, cmpanyspecific : resdb.caseStudy.anySpecificReq,
                    cmpcompanies : resdb.caseStudy.companies, cpmcandidates : resdb.caseStudy.candidates, closingdate : resdb.closedate,
                    companyemail:resdb.companyemail, companypassword:resdb.password,loginfrom:loginfrom,casestudyname : resdb.caseStudyName});
                        }
                    });
                
                    db.close();
                }
            });
        }else{
            res.redirect("/login");
        }
        }else{
            res.send(false);
        }
        
    }catch(err){
        console.log(err);
    }      
});

app.post('/sendmail',function(req, res){
    res.send(true);
    /* var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'youremail@gmail.com',
          pass: 'yourpassword'
        }
      });
      
      var mailOptions = {
        from: 'youremail@gmail.com',
        to: 'myfriend@yahoo.com',
        subject: 'Sending Email using Node.js',
        text: 'That was easy!'
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      }); */
});




app.get('/changepassword/:mailid',function(req, res){
    try{
        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
            var dbo = db.db(databasename);
            if(err){
                console.log(err);
            }else{
                dbo.collection('students').findOne({ "emailId": req.params.mailid},function(err, data){
                    if(err){
                        console.log(err);
                    }else{
                        if(data.password == ""){
                            res.render('changepassword',{'usermailid' : req.params.mailid});
                        }else{
                            res.render('login');
                        }
                    }
                    db.close();
                });
            }   
        });     
    }catch(err){
        console.log(err);
    }      
});


app.post("/changepassword",function(req, res){
    try{
        var password = req.body.txtpassword;
        var confirmpassword = req.body.txtconfirmpassword;
        var useremailid = req.body.useremail;
        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
            if(err){
                console.log(err);
            }else{
                var dbo = db.db(databasename);
                var oldvalue = {emailId : useremailid};
                var newvalue = {$set: {password : confirmpassword}}
                dbo.collection('students').updateMany(oldvalue, newvalue, function(err, data){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('%d docs update',data.result.nModified);
                        res.send(true);
                    }
                    db.close();
                });
            }
        });
    }catch(err){
        console.log(err);
    }  
});


app.post("/updatecompanyinfo",function(req, res){
    try{
        var companylogo = req.body.txtcomanylogo;
        console.log("logo name : "+companylogo);
        var data = companylogo.replace(/^data:image\/\w+;base64,/, "");
        var buf = new Buffer(data, 'base64');
        var val = Math.floor(1000 + Math.random() * 9000);
        var imagename = val+'image.png';
        fs.writeFile(__dirname+'/public/upload/companylogo/'+imagename, buf);

        var casestudyname = req.body.txtcasestudy;
        var name = req.body.txtcompanyname;
        var description = req.body.txtcdescription;
        var url = req.body.txtcurl;
        var statistics = req.body.txtcstatistics;
        var currentMarket = req.body.txtcurrentmarket;
        var BackgroundProblem = req.body.txtbackgroundproblem;
        var GeneralProblem = req.body.txtgenralproblem;
        var specificProblem = req.body.txtspecificproblem;
        var companies = req.body.txtcompanies;
        var candidates = req.body.txtcandidates;
        var caseCategary = req.body.txtcasecategory;
        var anySpecificReq = req.body.txtanyspecificsrequirment;
        var caseid = req.body.hiddencasid;
        var closedate = req.body.txtdatepicker;
        var companyemail = req.body.txtcompanyemail;
        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
            var dbo = db.db(databasename);
            if(err){
                console.log(err);
            }else{
                var oldrecord = {_id : ObjectId(caseid)};
                var newrecord = {$set : {'companylogo' : imagename, 'caseStudyName' : casestudyname ,"name" : name , 'description' : description, "url" : url, "statistics" : statistics, 'currentMarket' : currentMarket,
            'caseStudy.BackgroundProblem' : BackgroundProblem, 'caseStudy.GeneralProblem' : GeneralProblem, 'caseStudy.specificProblem' : specificProblem, 
            'caseStudy.companies' : companies, 'caseStudy.candidates' : companies,'caseStudy.caseCategary' : caseCategary ,
            'caseStudy.anySpecificReq' : anySpecificReq, 'closedate' : closedate, 'companyemail' : companyemail}};
                dbo.collection('tblcompanyinfo').updateMany(oldrecord, newrecord ,function(err, data){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('%d docs update',data.result.nModified);
                        res.send("/companyinfo/"+caseid);
                    }
                    db.close();
                });
            }   
        }); 
    }catch(err){
        console.log(err);
    }  
});


app.get("/admin",function(req, res){
    try{
        if( req.session.admin == "admin"){
            MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                var dbo = db.db(databasename);
                dbo.collection('tblcompanyinfo').find({}).toArray(function(err, resdb){
                    if(err){
                        console.log(err);
                    }else{   
                        res.render('admin',{casestudy : resdb});
                    }
                });
                db.close();    
            });
        }else{
            res.redirect("/login");
        } 
    }catch(err){
        console.log(err);
    }      
});


app.post("/updatestatus",function(req, res){
    try{
        var status = req.body.status;
        var caseid = req.body.casestudyid;
        console.log("update id : "+status+" "+caseid);
        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
            var dbo = db.db(databasename);
            if(err){
                console.log(err);
            }else{
                var oldrecord = {_id : ObjectId(caseid)};
                var newrecord = {$set : {status : status}};
                dbo.collection('tblcompanyinfo').updateMany(oldrecord, newrecord ,function(err, data){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('%d docs update',data.result.nModified);
                        res.send(true);
                    }
                    db.close();
                });
            }   
        }); 
    }catch(err){
        console.log(err);
    }  
});


app.get("/allusers/:id",function(req, res){
    try{
        if( req.session.admin == "admin"){
            if(ObjectId.isValid(req.params.id)){
                var allusersinfo = [];
                //console.log("case id : "+req.params.id);
                MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                    var dbo = db.db(databasename);
                    dbo.collection('tblcompanyinfo').findOne({_id : ObjectId(req.params.id)},function(err, resdb){
                        if(err){
                            console.log(err);
                        }else{   
                            for(var i = 0; i < resdb.applieduser.length;i++){
                                allusersinfo.push(ObjectId(resdb.applieduser[i].userid));
                            } 
                            var objectid = {_id :{ $in : allusersinfo}};
                            //var objectid = {_id :{ $in : [ObjectId('5abe4df1e1254d25b8959ff4'), ObjectId('5ac6228cb2d8ec4758df7715')]}};
                            dbo.collection('students').find(objectid).toArray(function(err, data){
                                if(err){
                                    console.log(err);
                                }else{  
                                    console.log("user data : "+data);
                                    res.render("allusers",{usersinfo : data, casid : req.params.id});
                                }
                            }); 
                            db.close();
                        // res.render("allusers",{usersinfo : allusersinfo, casid : req.params.id});
                        }
                    }); 
                    
                });
            }else{
                res.send(false);
            } 
        }
    }catch(err){
        console.log(err);
    }  
});

app.post("/selecteduserprofile",function(req, res){
    try{
        if( req.session.admin == "admin"){
            console.log(req.session.admin);
            req.session.profileregId = req.body.profileid;
            res.send(true);
        }else{
            res.send(false);
        }
    }catch(err){
        console.log(err);
    }  
});

app.post("/uploadresume",function(req, res){
    try{
        if(req.files && req.session.profileregId){
            var file = req.files.filename,
            filename = file.name;
            if(filename.split('.').pop() === 'pdf'){
                file.mv("./public/upload/resume/"+filename,function(err){
                    if(err){
                        console.log(err);
                    }else{
                        MongoClient.connect("mongodb://localhost:27017/", function(err, db){
                            if(err){
                                console.log(err);
                            }else{
                                var dbo = db.db(databasename);
                                var oldvalue = {_id : ObjectId(req.session.profileregId)};
                                var newvalue = {$set: {"resumePdf": filename}};
                                dbo.collection('students').updateMany(oldvalue, newvalue,function(err, data){
                                    if(err){
                                        console.log(err);
                                    }else{
                                        console.log('%d docs update',data.result.nModified);
                                        res.redirect("/profile/"+req.session.profileregId);
                                        
                                    }
                                });
                            }
                        });
                    }
                });
            }else{
                res.send("This file not pdf! upload only pdf files.");
            }
        }else{
            res.send(false);
        }
    }catch(err){
        console.log(err);
    }  
});

app.post("/editprofiledetails",function(req, res){
    var userfname = req.body.userfname;
    var userlname = req.body.userlname;
    var collegename = req.body.collegename;
    var streamname = req.body.streamname;
    var year = req.body.streamyear;
    var number = req.body.number;
    var linkedinprofileurl = req.body.linkedinprofiledetails;
    var objectid = req.body.userid;
    console.log(collegename+" "+streamname+" "+year+" "+number+" "+linkedinprofileurl);
    MongoClient.connect("mongodb://localhost:27017/", function(err, db){
        if(err){
            console.log(err);
        }else{
            var dbo = db.db(databasename);
            var oldvalue = {_id : ObjectId(objectid)};
            var newvalue = {$set: {'details.basicDetails.firstName' : userfname, 'details.basicDetails.lastName' : userlname, 'details.basicDetails.phone' : number,
        'details.basicDetails.linkedin' : linkedinprofileurl, 'details.educationalDetails.postgGrauationCollege' : collegename, 
        'details.educationalDetails.postgGrauationStream' : streamname, 'details.educationalDetails.postgGrauationYear' : year}};
            dbo.collection('students').updateMany(oldvalue, newvalue,function(err, data){
                if(err){
                    console.log(err);
                }else{
                    console.log('%d docs update',data.result.nModified);
                    res.send("/profile/"+objectid);
                }
            });
        }
    });
});

app.get('*', function(req, res, next) {
    var err = new Error();
    err.status = 404;
    next(err);
  });
   
  // handling 404 errors
  app.use(function(err, req, res, next) {
    if(err.status !== 404) {
      return next();
    }
   
    res.send(err.message || '** This page not available. **');
  });