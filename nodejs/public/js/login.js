

	function cheackfornull(controlvalue){
		if(controlvalue.trim() == ""){
			return false;
		}else{
			return true;
		}
	}

	function userlogin(){
		var emailid =  $("#txtemailid").val();
		var password = $("#txtpassword").val();
		var flag = true;
		if(!cheackfornull(emailid)){
			flag = false;
		}
		
		if(!cheackfornull(password)){
			flag = false;
		}
		
		if(flag){
			$.post("/login", { txtemailid: emailid, txtpassword: password}, function (e) {
				if(e){
					window.location.href=e;	
				}else{
					$("#spnerror").html("Username or password incorrect.");
				}
			});
		}else{
			$("#spnerror").html("Please enter email and password");
		}
		
	}

	function changepassword(){
		var confirmpassword =  $("#txtpassword").val();
		var password = $("#txtconfirmpassword").val();
		var emailid =  $("#hiddenemaild").val();
		
		
		if(confirmpassword != "" && password != "" && emailid != ""){
			if(confirmpassword == password){
				$.post("/changepassword", {txtconfirmpassword : confirmpassword,txtpassword : password, useremail : emailid}, function (e) {
					if(e){
						$("#spnerror").html("");
						window.location.href="/login";
					}
				});
			}else{
				$("#spnerror").html("Confirm password not match.");
			}
		}else{
			$("#spnerror").html("Please enter password and confirm password");
		}
		
	}