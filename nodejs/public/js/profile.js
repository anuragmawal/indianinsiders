var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var web = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
var phone = /^[0-9,+,-]+$/;
var linkeinurl = /^((https?:\/\/)?((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))$/;
$(document).ready(function() {

   $("#filechoose").click(function(){
	   $("#fileupload").show();
   });
   
    $("#fileupload").click(function(){
	   $("#filechoose").show();
	    $("#fileupload").hide();
   });
   
   $(".closebtnmodel").click(function(){
	   $(".modal").modal('hide');
   });
   
    $(".photo__canvas,.message--desktop").click(function(){
		$('#previewBtn').show();
		$('#previewBtnlogo').show();
   });
   
   $('body').on('hidden.bs.modal', '.modal', function () {
	  $(this).removeData('bs.modal');
	}); 

	$(".btnreadmore").click(function(){
		window.open('/compnayinfopdfpriew/'+ this.id, '_blank');	
	});

	$(".tabcompanyinfo").click(function(){
		window.open('/companyinfo/'+ this.id, '_blank');	
	});

	$(".selectuserprofile").click(function(){
		var userid = this.id;
		$.post("/selecteduserprofile",{profileid : userid},function(e){
			window.open('/profile/'+userid, '_blank');
		});	
	});

	$(".changestatus").click(function(){
		/*if(cheackforblank("txtcompanyname") != "" && cheackforblank("txtcdescription")  != "" && cheackforblank("txtcurl") != "" 
		&& cheackforblank("txtcstatistics") != "" && cheackforblank("txtcurrentmarket") != "" && cheackforblank("txtbackgroundproblem") != "" 
		&& cheackforblank("txtgenralproblem") != ""	&& cheackforblank("txtspecificproblem") != "" && cheackforblank("txtcompanyemail") != ""
		&& cheackforblank("txtcompanies") != "" && cheackforblank("txtcandidates") != "" && cheackforblank("txtcasecategory") != "" 
		&& cheackforblank("txtanyspecificsrequirment") != "" && cheackforblank("txtdatepicker") != ""){*/
			var caseid = $("#hiddencasid").val();
			var reportstatus = 2;
			if(this.id == "btnclose"){
				reportstatus = 5;
			}else if(this.id == "btnpublish"){
				reportstatus =3;
			}else if(this.id == "btncancel"){
				reportstatus = 4; 
			}
			$("#spnerrormsg").html("");
			
			$.post("/updatestatus", {status: reportstatus, casestudyid : caseid }, function (e) {
				if(e){
					alert("report send");
				}else{
					alert("error");
				}
			});
		/*}else{
			$("#spnerrormsg, #spnerrormsg1").html("Please fill all field.");
		}*/
	});
	
/*
	$("#btnsendreport").click(function(){
		var caseid = $("#hiddencasid").val();
		$.post("/updatestatus", {status: 2, casestudyid : caseid }, function (e) {
			if(e){
				alert("report send");
			}else{
				alert("error");
			}
		});
	});

	
	$("#btnclose").click(function(){
		var caseid = $("#inptucasestudy").val();
		$.post("/updatestatus", {status: 5, casestudyid : caseid }, function (e) {
			if(e){
				alert("report send");
			}else{
				alert("error");
			}
		});
	});
	
	$("#btnpublish").click(function(){
		var caseid = $("#inptucasestudy").val();
		$.post("/updatestatus", {status: 3, casestudyid : caseid }, function (e) {
			if(e){
				alert("report send");
			}else{
				alert("error");
			}
		});
	});
	
	$("#btncancel").click(function(){
		var caseid = $("#inptucasestudy").val();
		$.post("/updatestatus", {status: 4, casestudyid : caseid }, function (e) {
			if(e){
				alert("report send");
			}else{
				alert("error");
			}
		});
	});*/
	/*
	$("#uploadrsume").click(function(){
		var filename = $("#txtuploadresume").val();
		var userid = $("#inptucasestudy").val();
		$.post("/uploadresume", {file: filename, profileid : userid }, function (e) {
			if(e){
				alert("report send");
			}else{
				alert("error");
			}
		});
	});*/

	$("#btneditprofile").click(function(){
		$("#diveditprofile").show();
		$("#divpreviwprofile").hide();
		$("#btneditprofileclose").show();
	});

	$("#btneditprofileclose").click(function(){
		$("#diveditprofile").hide();
		$("#divpreviwprofile").show();
		$("#btneditprofileclose").hide();
	});
	
	$("#spncoutapplycasestudy").html($(".applycasestudy").length);

	$("#btneditprofileupdate").click(function(){
		var userfname = $("#txtuserfname").val();
		var userlname = $("#txtuserlname").val();
		var college = $("#txtusercollege").val(); 
		var stream = $("#txtgraduation").val();
		var year = $('input[name=year]:checked').val();
		var number = $("#txtphone").val();
		var linkedinprofile = $("#txtlinkedin").val(); 
		var userid = $("#hiddenuserid").val();
		var flag = true;
		if(!checkforblank(userfname, 'txtuserfname')){
			flag = false;
		}

		if(!checkforblank(userlname, 'txtuserlname')){
			flag = false;
		}

		if(!checkforblank(college, 'txtusercollege')){
			flag = false;
		}

		if(!checkforblank(stream, 'txtgraduation')){
			flag = false;
		}

		if(!checkforblank(number, 'txtphone')){
			flag = false;
		}else if(number.match(phone) && number.length > 6 && number.length < 14){
			$("#spntxtphone").html("");
		}else{
			$("#spntxtphone").html("Please enter valid phone no.");
			flag = false;
		}

		if(!checkforblank(linkedinprofile, 'txtlinkedin')){
			flag = false;
		}else if(!linkedinprofile.match(linkeinurl)){
			$("#spntxtlinkedin").html("Please enter valid linkedin url.");
			flag = false;
		}

		if(flag){
			$.post("/editprofiledetails",{userid : userid, userfname : userfname, userlname: userlname, collegename : college, streamname : stream, streamyear : year, number : number, linkedinprofiledetails : linkedinprofile},
			function(e){
				if(e){
					$("#diveditprofile").hide();
					$("#divpreviwprofile").show();
					$(".profilename").html(userfname.substring(0, 1));
					$("#spnfullname").html(userfname+" "+userlname);
					$("#divcollegename").html(college); 
					$("#spnspecialization").html(stream);
					if(year == 1){
						$("#divyear").html('1<sup>st</sup> year');
					}else if(year == 2){
						$("#divyear").html('2<sup>nd</sup> year');
					}
					$("#divcontactnumer").html(number);
					$("#divlinkedinurl").html(linkedinprofile); 
				}
			});
		}
	});
});

function cheackforblank(controlid){
	var controlvalue = $("#"+controlid).val();
	if(controlvalue == "" || $.trim(controlvalue) == ""){
		return false;
	}else{
		return true;
	}
}

function saveimageurl(getimagurl){
	var imgurl = getimagurl;
	$('#profilepicmodel').modal('hide');
	$.post("ajax/userdetails.php", {ajximgurl: imgurl}, function (e) {
		$("#imgdiv").empty();
		$("#imgdiv").append("<img src="+e+" style='border-radius: 150px;'>");
	});
}

function logoutprofile(){
	$.post("/logout", function (e) {
		if(e){
			window.location.href = "/login";
		}	
	});
}
/*
function formatDate(date) {
	var monthNames = [
	  "January", "February", "March",
	  "April", "May", "June", "July",
	  "August", "September", "October",
	  "November", "December"
	];
  
	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();
  
	alert( day + ' ' + monthNames[monthIndex] + ' ' + year);
  }


  */

  function editcompanyinfor(getid){
	  var caseid = getid;
	 window.location.href="/companyinfo/"+getid;
}

function openapplyusers(caseid){
	window.location.href="/allusers/"+caseid;
}


/*
function updatecompanyinfo(){
	var name = $("#txtcompanyname").val();
	var description = $("#txtcdescription").val();
	var url = $("#txtcurl").val();
	var statistics = $("#txtcstatistics").val();
	var currentmarket = $("#txtcurrentmarket").val();
	var backgroundproblem = $("#txtbackgroundproblem").val();
	var generalproblem = $("#txtgenralproblem").val();
	var specificproblem = $("#txtspecificproblem").val();
	var companies = $("#txtcompanies").val();
	var candidate = $("#txtcandidates").val();
	var casecategary = $("#txtcasecategory").val();
	var anyspecificreq = $("#txtanyspecificsrequirment").val();

	$.post("/updatecompanyinfo",{txtcompanyname : name, description: txtcdescription, url: txtcurl, statistics:txtcstatistics, currentmarket: txtcurrentmarket,
		backgroundproblem:txtbackgroundproblem,generalproblem:txtgenralproblem, specificproblem:txtspecificproblem, companies:txtcompanies,
		candidate:candidate,casecategary:txtcasecategory,anyspecificreq:txtanyspecificsrequirment}, function (e) {
		alert(e);
	});

}
*/

function createnewcompanyinfo(){
	var companylogo = $("#txtcompanylogo").val();
	var casestudyname = $("#txtcasestudy").val();
	var name = $("#txtcompanyname").val();
	var description = $("#txtcdescription").val();
	var url = $("#txtcurl").val();
	var statistics = $("#txtcstatistics").val();
	var currentmarket = $("#txtcurrentmarket").val();
	var backgroundproblem = $("#txtbackgroundproblem").val();
	var generalproblem = $("#txtgenralproblem").val();
	var specificproblem = $("#txtspecificproblem").val();
	var companies = $("#txtcompanies").val();
	var candidate = $("#txtcandidates").val();
	var casecategary = $("#txtcasecategory").val();
	var anyspecificreq = $("#txtanyspecificsrequirment").val();
	var companymail = $("#txtcompanyemail").val();
	var password = $("#companypassword").val();
	var closingdate = $("#txtdatepicker").val();
	var flag = true;
	
	if(companylogo == ""){
		$("#spntxtcompanylogo").html("Please upload company logo.");
		flag = false;
	}else{
		flag = true;
		$("#spntxtcompanylogo").html("");
	}

	if(!checkforblank(casestudyname, 'txtcasestudy')){
		flag = false;
	}

	if(!checkforblank(name, 'txtcompanyname')){
		flag = false;
	}

	if(!checkforblank(companymail, 'txtcompanyemail')){
		flag = false;
	}else if(!companymail.match(re)){
		$("#spntxtcompanyemail").html("Please enter correct mail id.");
		flag = false;
	}

	if(!checkforblank(closingdate, 'txtdatepicker')){
		flag = false;
	}else{
		var inputDate = new Date(closingdate);
		var todaysDate = new Date();
		if(inputDate < todaysDate) {
			$("#spntxtdatepicker").html("Please select correct date.");
			flag = false;
		}
	}

	if(flag){
		$.post("/companyinfo",{txtcomanylogo : companylogo, txtcasestudy : casestudyname , txtcompanyname : name, txtcdescription : description, txtcurl : url, txtcstatistics : statistics, txtcurrentmarket : currentmarket,
			txtbackgroundproblem : backgroundproblem,txtgenralproblem : generalproblem, txtspecificproblem : specificproblem, txtcompanies : companies,
			txtcandidates:candidate, txtcasecategory : casecategary, txtanyspecificsrequirment : anyspecificreq, txtcompanyemail:companymail, companypassword:password, txtdatepicker : closingdate}, function (e) {
			if(e){
				window.location.href=e;
			}else{
				alert("some thing wrong");
			}	
		});
	}

}



function updatecompanyinfo(){
	var flag = true;
	var companylogo = $("#txtcompanylogo").val();
	var casestudyname = $("#txtcasestudy").val();
	var name = $("#txtcompanyname").val();
	var description = $("#txtcdescription").val();
	var url = $("#txtcurl").val();
	var statistics = $("#txtcstatistics").val();
	var currentmarket = $("#txtcurrentmarket").val();
	var backgroundproblem = $("#txtbackgroundproblem").val();
	var generalproblem = $("#txtgenralproblem").val();
	var specificproblem = $("#txtspecificproblem").val();
	var companies = $("#txtcompanies").val();
	var candidate = $("#txtcandidates").val();
	var casecategary = $("#txtcasecategory").val();
	var anyspecificreq = $("#txtanyspecificsrequirment").val();
	var companymail = $("#txtcompanyemail").val();
	var password = $("#companypassword").val();
	var closingdate = $("#txtdatepicker").val();
	var casestudyid = $("#hiddencasid").val();
	
	if(companylogo == ""){
		$("#spntxtcompanylogo").html("Please upload company logo.");
		flag = false;
	}else{
		flag = true;
		$("#spntxtcompanylogo").html("");
	}

	if(!checkforblank(casestudyname, 'txtcasestudy')){
		flag = false;
	}

	if(!checkforblank(name, 'txtcompanyname')){
		flag = false;
	}

	if(!checkforblank(casestudyname, 'txtcasestudy')){
		flag = false;
	}

	if(!checkforblank(description, 'txtcdescription')){
		flag = false;
	}

	if(!checkforblank(url, 'txtcurl')){
		flag = false;
	}else if(!url.match(web)){
		$("#spntxtcurl").html("Please enter correct url.");
		flag = false;
	}


	if(!checkforblank(statistics, 'txtcstatistics')){
		flag = false;
	}

	if(!checkforblank(currentmarket, 'txtcurrentmarket')){
		flag = false;
	}

	if(!checkforblank(backgroundproblem, 'txtbackgroundproblem')){
		flag = false;
	}

	if(!checkforblank(generalproblem, 'txtgenralproblem')){
		flag = false;
	}

	if(!checkforblank(specificproblem, 'txtspecificproblem')){
		flag = false;
	}

	if(!checkforblank(companies, 'txtcompanies')){
		flag = false;
	}

	if(!checkforblank(candidate, 'txtcandidates')){
		flag = false;
	}

	if(!checkforblank(casecategary, 'txtcasecategory')){
		flag = false;
	}
	
	if(!checkforblank(anyspecificreq, 'txtanyspecificsrequirment')){
		flag = false;
	}

	if(!checkforblank(companymail, 'txtcompanyemail')){
		flag = false;
	}else if(!companymail.match(re)){
		$("#spntxtcompanyemail").html("Please enter correct mail id.");
		flag = false;
	}

	if(!checkforblank(closingdate, 'txtdatepicker')){
		flag = false;
	}else{
		var inputDate = new Date(closingdate);
		var todaysDate = new Date();
		if(inputDate < todaysDate) {
			$("#spntxtdatepicker").html("Please select correct date.");
			flag = false;
		}
	}

	if(flag){
		$.post("/updatecompanyinfo",{txtcomanylogo : companylogo, txtcasestudy : casestudyname ,txtcompanyname : name, txtcdescription : description, txtcurl : url, txtcstatistics : statistics, txtcurrentmarket : currentmarket,
			txtbackgroundproblem : backgroundproblem,txtgenralproblem : generalproblem, txtspecificproblem : specificproblem, txtcompanies : companies,
			txtcandidates:candidate, txtcasecategory : casecategary, txtanyspecificsrequirment : anyspecificreq, txtcompanyemail:companymail,
			hiddencasid:casestudyid, txtdatepicker : closingdate}, function (e) {
			window.location.href=e;
		});
	}	
}

function checkforblank(controlvalue, errorid){
	if(controlvalue != "" && controlvalue.trim() !== ""){
		$("#spn"+errorid).html("");
		return true;
	}else{
		$( "#"+errorid ).focus();
		$("#spn"+errorid).html("Please fill the blank field.");
		return false;
	}
}

/*
function uploadresume(){
	var data;
    data = new FormData();
	data.append( 'file', $( '#filename' )[0].files[0] );
	alert(data);
}
*/