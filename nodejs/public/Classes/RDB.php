<?php 
include_once('Settings.php');
class RDB
{
public $DbName;
public $DbPassword;
public $DbUser;
public $Hostname;
public $Connection;

/*public function RDB(){
	echo $DBNAME ;
	
}*/

/*function __construct()
{
	echo 'Constructor called';
	$this->DbName = DBNAME ;
	$this->DbPassword = DBPASSWORD;
	$this->DbUser = DBUSER;
	$this->Hostname = HOSTNAME;
}*/

function __construct1($conn)
{
	echo 'Constructor 1 called';
	$this->DbName = DBNAME ;
	$this->DbPassword = DBPASSWORD;
	$this->DbUser = DBUSER;
	$this->Hostname = HOSTNAME;
	$this->Connection = $conn;
}

function RDB($conn = null)
{
	$this->DbName = DBNAME ;
	$this->DbPassword = DBPASSWORD;
	$this->DbUser = DBUSER;
	$this->Hostname = HOSTNAME;
	if($conn != null)
		$this->Connection = $conn;
}

function connect()
{
	$MySqli = new MySqli($this->Hostname, $this->DbUser, $this->DbPassword, $this->DbName);

	if($MySqli->connect_errno)
	{
		//printf('connection failed: %s \n', $MySqli->connect_errno);
		return false;
	}
	else
	{
    if($this->Connection != null)
    {
    return true;
}else{
		$this->Connection = mysqli_connect($this->Hostname, $this->DbUser, $this->DbPassword, $this->DbName);
		//echo "Conneted";	
		return true;
	}}
	
	/*$DbConnection = mysqli_connect($this->Hostname, $this->DbUser, $this->DbPassword, $this->DbName);
	if(mysqli_errno($DbConnection))
		die('DB failed');
	else
		{		
		echo 'connected';
		}*/
}

function connect1()
{
	$MySqli = new MySqli($this->Hostname, $this->DbUser, $this->DbPassword, $this->DbName);

	if($MySqli->connect_errno)
	{
		//printf('connection failed: %s \n', $MySqli->connect_errno);
		return false;
	}
	else
	{
		$this->Connection = mysqli_connect($this->Hostname, $this->DbUser, $this->DbPassword, $this->DbName);
		//echo "Conneted";	
		return $this->Connection;
	}
	
	/*$DbConnection = mysqli_connect($this->Hostname, $this->DbUser, $this->DbPassword, $this->DbName);
	if(mysqli_errno($DbConnection))
		die('DB failed');
	else
		{		
		echo 'connected';
		}*/
}

function IsDatabaseConnectionExists()
{
	if(!$this->connect())
	{
		if(mysqli_connect_errno()){
			
			die("Database Connection failed: " . 
				mysqli_connect_error() . "(" . 
				mysqli_connect_errno() . ")" );
			return false;
		}
	}
	else
		return true;
}

function Execute($Query)
{
	if($this->connect())
	{			
		$result =	mysqli_query($this->Connection, $Query);
		return $result;
	}
	else
	{
		return "Not able to connect to Database";
	}
}
function ExecuteWithGlobalConn($Query)
{			
		$result =	mysqli_query($this->Connection, $Query);
		return $result;
}

function Close()
{
	mysqli_close($this->Connection);
}

function ExecuteInsert($Query)
{
    if($this->connect())
	{			
		$result =	mysqli_query($this->Connection, $Query);
        $lastInsertId = mysqli_insert_id($this->Connection);
		return $lastInsertId;
	}
	else
	{
		return 0;
	}
}
function ExecuteMultiQuery($Query)
{
	if($this->connect())
	{			
		$result =	mysqli_multi_query($this->Connection, $Query);
		return $result;
	}
	else
	{
		return "Not able to connect to Database";
	}
}

}
?>