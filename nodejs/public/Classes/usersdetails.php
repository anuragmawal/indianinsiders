<?php
class userdetails{
	public $usermailid;
	public $username;
	public $usercolleg;
	public $userstream;
	public $userstreamyear;
	public $userlastname;
	public $compmailid;
	public $fname;
	public $location;
	public $industry;
	public $message;
	public $phone;
	public $website;
	public $lname;
	public $login;
	public $password;
	
	function Insertuserdetails($userid){
		$rdbobj = new RDB();
		if($userid == ""){
			$query = "INSERT INTO master_userdetails(emailid, firstname, lastname, collegename, stream, year, createdby, createdon, modifiedby, modifiedon, isdeleted) VALUES ('{$this->usermailid}', '{$this->username}', '{$this->userlastname}', '{$this->usercolleg}', '{$this->userstream}',{$this->userstreamyear}, 1,now(), 1,now(),0)";
				$selectduserid = $rdbobj->ExecuteInsert($query);
				if($selectduserid != ""){
					$this->sendmail($this->usermailid); 
					return $selectduserid;
				}else{
					return false;
				}	
		}else{
			
			$updatequery = "UPDATE master_userdetails SET emailid='{$this->usermailid}' ,firstname='{$this->username}' ,lastname='{$this->userlastname}', collegename='{$this->usercolleg}', stream='{$this->userstream}', year={$this->userstreamyear}, createdby={$userid} ,createdon= now(), modifiedby={$userid}, `modifiedon`= now(), isdeleted=0 WHERE userid = {$userid}";
			$result = $rdbobj->Execute($updatequery);
			if($result){
				return $userid;
			}else{
				return false;
			}
		}	
			
	}
	
	function Insertcompanydetails($cuserid){
		$rdbobj = new RDB();
		if($cuserid == ""){
			$query = "INSERT INTO master_companydetails (firstname, lastname, emailid, phonenumber, website, location, industry, remarks, createdby, createdon, modifiedby, modifiedon, isdeleted) VALUES ('{$this->fname}','{$this->lname}','{$this->compmailid}','{$this->phone}', '{$this->website}', '{$this->location}', '{$this->industry}', '{$this->message}', 1, now(), 1, now(), 0)";
		
				$selectduserid = $rdbobj->ExecuteInsert($query);
				if($selectduserid != ""){
					$this->sendcompanymail($this->compmailid); 
					return $selectduserid;
				}else{
					return false;
				}	
		}else{
			
			$updatequery = "UPDATE master_companydetails SET emailid='{$this->compmailid}' ,firstname='{$this->fname}' ,lastname='{$this->lname}', phonenumber='{$this->phone}', website='{$this->website}', location='{$this->location}', industry='{$this->industry}', remarks='{$this->message}', createdby={$cuserid} ,createdon= now(), modifiedby={$cuserid}, `modifiedon`= now(), isdeleted=0 WHERE companyuserid = {$cuserid}";

			$result = $rdbobj->Execute($updatequery);
			if($result){
				return $cuserid;
			}else{
				return false;
			}
		}
	}

	function sendmail($mailid){
		 $to = $mailid;
		 $subject = "Thank you for registering at IndianInsiders";
		 
		 $message = "<p><b>Dear Future Consultants,</b><p>";
		 $message .= "<p>Thank you for expressing your interest in the Indian Insiders program.</p>";
		 $message .= "<p>This is a confirmation that you have registered successfully. Your details are now under our evaluation process.</p>";
		 $message .= "<p>Once confirmed we will send you an official invite and after that we can start working on the case studies.</p>";
		 $message .= "<p>If you have any queries or inquiries, drop me an email at anurag@indianinsiders.com and I will get back to you.</p></br></br>";
		 $message .= "<p><b>Best regards,</b></p><p><b>Anurag Mawal</b></p><p><b>CEO - IndianInsiders</b></p>";
		 $header="";
		 $header .= "MIME-Version: 1.0\r\n";
		 $header .= "Content-type: text/html\r\n";
		 $header .= "From: anurag@indianinsiders.com" . "\r\n";
		 $header .= "Reply-To: anurag@indianinsiders.com";

			$result = mail($to, $subject, $message, $header);
			/*if($result){
				$txt = "{$mailid} true";
			}else{
				$txt = "{$mailid} false";
			}*/
			//$myfile = file_put_contents('Usersmailfile.txt', $txt.PHP_EOL , FILE_APPEND);
		 
	}
	
	function sendcompanymail($mailid){
		 $to = $mailid;
		 $subject = "Thank you for registering at IndianInsiders";
		 
		 $message = "<p><b>Dear Future Consultants,</b><p>";
		 $message .= "<p>Thank you for expressing your interest in the Indian Insiders program.</p>";
		 $message .= "<p>This is a confirmation that you have registered successfully. Your details are now under our evaluation process.</p>";
		 $message .= "<p>Once confirmed we will send you an official invite and after that we can start working on the case studies.</p>";
		 $message .= "<p>If you have any queries or inquiries, drop me an email at anurag@indianinsiders.com and I will get back to you.</p></br></br>";
		 $message .= "<p><b>Best regards,</b></p><p><b>Anurag Mawal</b></p><p><b>CEO - IndianInsiders</b></p>";
		 $header ="";
		 $header .= "MIME-Version: 1.0\r\n";
		 $header .= "Content-type: text/html\r\n";
		 $header .= "From: anurag@indianinsiders.com" . "\r\n";
		 $header .= "Reply-To: anurag@indianinsiders.com";

			$result = mail($to, $subject, $message, $header);
			/*if($result){
				$txt = "{$mailid} true";
			}else{
				$txt = "{$mailid} false";
			}*/
			//$myfile = file_put_contents('Usersmailfile.txt', $txt.PHP_EOL , FILE_APPEND);
		 
	}
	

	function setusercontactusmail($username, $mailid, $subject, $message){
		 $to = "anurag@indianinsiders.com,anuragmawal@gmail.com,ketcia@indianinsiders.com,Ketcia.thach@gmail.com";
		 $subject = "Indian Insider Query";
		 $message = "<p><b>Subject :</b> ".$subject."<p>";
		 $message .= "<p><b>Name :</b> ".$username."<p>";
		 $message .= "<p><b>Email :</b> ".$mailid."<p>";
		 $message .= "<p><b>Description :</b>".$message."</p></b>";
		 $header ="";
		 $header .= "MIME-Version: 1.0\r\n";
		 $header .= "Content-type: text/html\r\n";
		 $header .= "From: anurag@indianinsiders.com" . "\r\n";
		 $header .= "Reply-To: anurag@indianinsiders.com";

			$result = mail($to, $subject, $message, $header);
			if($result){
				return true;
			}else{
				return false;
			}
	}
	
	function saveimageurl($imageurl){
		$data = $imageurl;
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
		$mydate=getdate(date("U"));
		$timezone = date_default_timezone_get();
		$imagename = 'upload/profilepic/img'.uniqid().'.jpg';
		file_put_contents("../".$imagename, $data);
		return $imagename;
	}
	
	
}

?>