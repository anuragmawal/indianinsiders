var fs = require('fs');
var http = require('http');

http.createServer(function(req,res) {
	console.log('Requested url : '+req.url);
	if(req.url === '/index' || req.url === '/'){
		res.writeHead(200, { 'Content-type':'text/html'});
		fs.createReadStream('index.html').pipe(res);
	}else if(req.url === '/about' || req.url === '/'){
		res.writeHead(200, { 'Content-type':'text/html'});
		fs.createReadStream('about.html').pipe(res);
	}else{
		res.writeHead(404,{'Content-type':'text/html'});
		fs.createReadStream('error.html').pipe(res);
	}
}).listen(3000);

console.log('server in runing.');